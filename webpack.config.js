/* eslint-disable @typescript-eslint/no-var-requires */
const path = require("path");
const webpack = require("webpack");
const CopyPlugin = require("copy-webpack-plugin");

const getEnv = () => {
    return {
        ...process.env,
        buildTime: Date.now(),
    };
};

module.exports = {
    entry: {
        app: "./src/main.ts",
        vendors: ["phaser"],
    },
    output: {
        filename: "app.bundle.js",
        path: path.resolve(__dirname, "dist"),
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/,
            },
            {
                test: require.resolve("Phaser"),
                loader: "expose-loader",
                options: { exposes: { globalName: "Phaser", override: true } },
            },
        ],
    },
    resolve: {
        modules: [path.resolve("./src"), path.resolve("./node_modules")],
        extensions: [".ts", ".tsx", ".js"],
    },
    mode: process.env.NODE_ENV == "production" ? "production" : "development",
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        port: 3000,
        writeToDisk: true,
        open: true,
        host: "192.168.0.71", //your ip address
        disableHostCheck: true,
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: "index.html",
                },
                {
                    from: "src/assets/**/*",
                    to({ context, absoluteFilename }) {
                        return absoluteFilename.replace('src', 'dist')
                    },
                },
                {
                    from: "src/styles/**/*",
                    to({ context, absoluteFilename }) {
                        return absoluteFilename.replace('src', 'dist')
                    },
                },
            ],
        }),
        new webpack.DefinePlugin({
            "typeof CANVAS_RENDERER": JSON.stringify(true),
            "typeof WEBGL_RENDERER": JSON.stringify(true),
            "process.env": JSON.stringify(getEnv() || "development"),
        }),
    ],
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "vendors",
                    chunks: "all",
                    filename: "[name].app.bundle.js",
                },
            },
        },
    },
};
