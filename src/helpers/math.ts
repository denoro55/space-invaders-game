import { Vector } from "components";

export const getRandomInt = (from: number, to: number) => {
    return Math.round(from + Math.random() * (to - from));
};

export const getDistanceBetweenPoints = (pos1: Vector, pos2: Vector) => {
    var a = pos1.x - pos2.x;
    var b = pos1.y - pos2.y;

    return Math.sqrt(a * a + b * b);
};
