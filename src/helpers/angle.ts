import { Vector } from "components";

// returns number in radians
export function getAngleBetween(obj1: Vector, obj2: Vector) {
    const xDiff = obj1.x - obj2.x;
    const yDiff = obj1.y - obj2.y;

    let result = Math.atan2(yDiff, xDiff);

    if (result < 0) result += Math.PI * 2;

    return result;
}

export function angleToDegrees(angle: number) {
    return (angle / Math.PI) * 180;
}

export function angleToRadians(angle: number) {
    return (angle * Math.PI) / 180;
}

export function getNearestAngle(currentAngle: number, targetAngle: number, step: number): number {
    if (Math.abs(currentAngle - targetAngle) < 2) return currentAngle;

    if (Math.abs(currentAngle - targetAngle) < 180) {
        // Rotate current directly towards target.
        if (currentAngle < targetAngle) currentAngle += step;
        else currentAngle -= step;
    } else {
        // Rotate the other direction towards target.
        if (currentAngle < targetAngle) currentAngle -= step;
        else currentAngle += step;
    }

    return ((currentAngle % 360) + 360) % 360;
}

export const getOffsetToAngle = (radians: number, distance: number) => {
    return new Vector(Math.cos(radians) * distance, Math.sin(radians) * distance);
};

export const getAngleDiff = (angle1: number, angle2: number) => {
    const a = Math.abs(angle1 - angle2);

    return Math.abs(((a + 180) % 360) - 180);
};

export const spreadAngle = (angle: number, spread: number) => {
    return angle - spread + Math.random() * (spread * 2);
};

export const spreadRadians = (radians: number, rangeAngle: number) => {
    return spreadAngle(radians, angleToRadians(rangeAngle));
};
