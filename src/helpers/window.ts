import { isMobile } from "config";

export const isLandscapeOrientation = () => {
    return isMobile && window.innerHeight < window.innerWidth;
};

export const getCurrentOrientation = (): TOrientation => {
    return isLandscapeOrientation() ? "horizontal" : "vertical";
};

export const isMobileDevice = () => {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
};
