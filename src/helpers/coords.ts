import { Vector } from "components";

export const getCenterBody = (
    width: number,
    height: number,
    scale: number,
    bodyWidth: number,
    offset: Vector = { x: 0, y: 0 }
) => {
    const x = width / scale / 2 - bodyWidth + offset.x;
    const y = height / scale / 2 - bodyWidth + offset.y;

    return new Vector(x, y);
};
