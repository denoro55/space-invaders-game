function plural(num: number) {
    if (num % 10 === 1 && num % 100 !== 11) {
        return 0;
    }
    if (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20)) {
        return 1;
    }
    return 2;
}

/* возвращает одну из строк в массиве в соответствии с остатком от деления
0 товар[ов]
1 товар
2 товар[а]
*/
export function pluralize(num: number, strings: string[]) {
    switch (plural(num)) {
        case 0:
            return strings[0];
        case 1:
            return strings[1];
        default:
            return strings[2];
    }
}
