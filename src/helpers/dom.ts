const rootElement = document.querySelector(".screen-height-detector");

export const showOrientationWarning = () => {
    document.body.classList.add("portrait-orientation-warning");
};

export const hideOrientationWarning = () => {
    document.body.classList.remove("portrait-orientation-warning");
};

export const showFullscreen = (canvas: HTMLCanvasElement, isShow = true) => {
    if (isShow && !isFullScreenMode()) {
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen({
                navigationUI: "hide",
            });
        } else if (canvas.webkitRequestFullscreen) {
            /* Safari */
            canvas.webkitRequestFullscreen({
                navigationUI: "hide",
            });
        } else if (canvas.msRequestFullscreen) {
            /* IE11 */
            canvas.msRequestFullscreen({
                navigationUI: "hide",
            });
        }
    } else if (!isShow && isFullScreenMode()) {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) {
            /* Safari */
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            /* IE11 */
            document.msExitFullscreen();
        }
    }
};

export const getFullWindowHeight = () => {
    return rootElement.getBoundingClientRect().height;
};

export const isFullScreenMode = () => {
    if (
        document.fullscreenElement /* Standard syntax */ ||
        document.webkitFullscreenElement /* Safari and Opera syntax */ ||
        document.msFullscreenElement /* IE11 syntax */
    ) {
        return true;
    }

    return false;
};
