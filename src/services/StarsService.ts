import { Star } from "objects/other/Star";
import { GameScene } from "scenes/Game";

import { Service } from "./Service";

export interface IStarsServiceParams {
    count: number;
}

export class StarsService extends Service {
    params: IStarsServiceParams;
    stars: Star[] = [];

    constructor(scene: GameScene, params: IStarsServiceParams) {
        super(scene);
        this.params = params;

        this.init();
    }

    init() {
        const { scene } = this;
        const { count } = this.params;

        for (let i = 0; i < count; i++) {
            this.stars.push(
                new Star(scene, {
                    initialAlphaCoef: i * 500,
                })
            );
        }
    }

    act(time: number, delta: number) {
        this.update(time, delta);
    }

    update(time: number, delta: number) {
        this.stars.forEach((star) => {
            star.act(time, delta);
        });
    }
}
