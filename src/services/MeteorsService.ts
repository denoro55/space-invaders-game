import { CONFIG } from "config";
import { Vector } from "components";
import { Meteor } from "objects/other/Meteor";
import { GameScene } from "scenes/Game";
import { angleToRadians } from "helpers/angle";
import { getRandomInt } from "helpers";

import { Service } from "./Service";

interface IMeteorsServiceParams {}

export class MeteorsService extends Service {
    params: IMeteorsServiceParams;
    meteors: Meteor[] = [];

    private meteorIntervalTime: number = 1000;
    private meteorIntervalTimeRange: number = 1000;
    private lastMeteorTime: number = 0;
    private maxCount: number = 25;

    constructor(scene: GameScene, params: IMeteorsServiceParams) {
        super(scene);
        this.params = params;

        this.init();
    }

    init() {
        for (let i = 0; i < getRandomInt(5, 8); i++) {
            this.spawnMeteor({
                outside: false,
            });
        }

        for (let i = 0; i < getRandomInt(3, 5); i++) {
            this.spawnMeteor({
                outside: true,
            });
        }
    }

    act(time: number, delta: number) {
        this.update(time, delta);
    }

    update(time: number, delta: number) {
        const gameSize = this.scene.getGameSize();

        const { scene, maxCount } = this;
        const deltaSpeed = delta / CONFIG.scene.delta;
        const gameTime = scene.gameTime;

        if (gameTime > this.lastMeteorTime && this.meteors.length < maxCount) {
            this.spawnMeteor({
                outside: true,
            });
        }

        this.meteors.forEach((meteor) => {
            const { body, speed } = meteor;

            body.angle += speed.x * 0.5 * deltaSpeed;
            body.x += speed.x * deltaSpeed;
            body.y += speed.y * deltaSpeed;

            if (body.x > gameSize.width + 200 || body.y > gameSize.height + 200) {
                this.meteors = this.meteors.filter((m) => m !== meteor);
                body.destroy();
            }
        });
    }

    spawnMeteor({ outside }: { outside: boolean }) {
        const { scene, meteorIntervalTime, meteorIntervalTimeRange } = this;
        const time = scene.gameTime;
        const scale = 0.5 + Math.random() * 1;
        const gameSize = this.scene.getGameSize();

        const startOffset = 200;
        const angle = angleToRadians(45);
        const speedValue = 0.25 + Math.random() * 2.5;
        const speed = new Vector(speedValue * Math.cos(angle), speedValue * Math.sin(angle));

        const axis = Math.random() > 0.5 ? "x" : "y";

        const spawnPoint = outside
            ? {
                  x: axis === "x" ? -startOffset + Math.random() * (gameSize.width * 0.75) : -startOffset,
                  y: axis === "y" ? -startOffset + Math.random() * (gameSize.height * 0.75) : -startOffset,
              }
            : {
                  x: gameSize.width * Math.random(),
                  y: gameSize.height * Math.random(),
              };

        this.meteors.push(
            new Meteor(scene, spawnPoint, speed, {
                spriteIndex: 1 + Math.floor(Math.random() * 4),
                alpha: 0.25 + Math.random() * 0.4,
                scale,
            })
        );

        this.lastMeteorTime = Math.random() * meteorIntervalTimeRange + time + meteorIntervalTime * scale;
    }
}
