import { CONFIG } from "config";
import { Comet } from "objects/other/Comet";
import { GameScene } from "scenes/Game";

import { Service } from "./Service";

export interface ICometsServiceParams {}

export class CometsService extends Service {
    params: ICometsServiceParams;
    comets: Comet[] = [];

    private count: number = 15;

    constructor(scene: GameScene, params: ICometsServiceParams) {
        super(scene);
        this.params = params;

        this.init();
    }

    init() {
        const { scene, count } = this;

        for (let i = 0; i < count; i++) {
            this.comets.push(new Comet(scene, {}));
        }
    }

    act(time: number, delta: number) {
        this.update(time, delta);
    }

    update(time: number, delta: number) {
        const gameSize = this.scene.getGameSize();
        const deltaSpeed = delta / CONFIG.scene.delta;

        this.comets.forEach((comet) => {
            const { body } = comet;

            body.y += comet.speed * deltaSpeed;

            if (body.y > gameSize.height + 100) {
                comet.updateParams();
                comet.refreshPosition();
            }
        });
    }
}
