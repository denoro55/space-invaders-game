import { GameScene } from "scenes/Game";
import { Service } from "./Service";

interface ILoggerServiceParams {}

export class LoggerService extends Service {
    params: ILoggerServiceParams;
    private lastLogTime: number = 0;

    constructor(scene: GameScene, params: ILoggerServiceParams) {
        super(scene);
        this.params = params;

        this.init();
    }

    init() {}

    act() {
        this.update();
    }

    update() {
        const scene = this.scene;
        const time = scene.gameTime;

        if (time > this.lastLogTime) {
            console.group("Stats");

            console.log(`effects: ${scene.effects.length}`);
            console.log(`enemies: ${scene.enemies.length}`);
            console.log(`bullets: ${scene.bullets.length}`);

            console.groupEnd();

            this.lastLogTime = time + 2000;
        }
    }
}
