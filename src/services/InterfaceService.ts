import { CONFIG, isMobile } from "config";
import { Vector } from "components";
import { COLORS } from "constants/colors";
import { UICosmons } from "ui";
import { GameScene } from "scenes/Game";

import { Service } from "./Service";

const HEALTHBAR_WIDTH = 200;
const GRAPHICS_DEPTH = 500;

interface IInterfaceServiceParams {}

export class InterfaceService extends Service {
    params: IInterfaceServiceParams;
    graphics: Phaser.GameObjects.Graphics;

    private counterText: Phaser.GameObjects.BitmapText;
    private scoreText: UICosmons;
    private playerHealthbar: Phaser.Geom.Rectangle;

    private backgroundInitialAlpha: number = 0;
    private backgroundChangeTime: number = 15000;

    constructor(scene: GameScene, params: IInterfaceServiceParams) {
        super(scene);
        this.params = params;

        this.init();
    }

    init() {
        const { scene } = this;
        const gameSize = this.scene.getGameSize();
        const moneyStartPosX = HEALTHBAR_WIDTH + 15;
        const textPosY = 27;

        this.graphics = scene.add.graphics({
            lineStyle: { width: 2, color: 0x00ff00 },
            fillStyle: { color: 0xfffaf6 },
        });

        this.counterText = scene.add
            .bitmapText(gameSize.width - 17, textPosY, "atari", "0", 24)
            .setOrigin(1, 0.5)
            .setDepth(GRAPHICS_DEPTH);

        this.scoreText = new UICosmons(scene, {
            pos: new Vector(moneyStartPosX + 28, textPosY),
            value: 0,
            isCentered: false,
            depth: 100,
        });

        this.playerHealthbar = new Phaser.Geom.Rectangle(20, 24, gameSize.width, 11);
        this.graphics.setDepth(GRAPHICS_DEPTH);

        this.backgroundInitialAlpha = Math.random() * (this.backgroundChangeTime * 5);

        if (isMobile) {
            this.initTouchAreas();
        }
    }

    initTouchAreas() {
        const gameSize = this.scene.getGameSize();

        this.scene.add
            .image(CONFIG.touchAreaOffset, gameSize.height - CONFIG.touchAreaOffset, "touch-area")
            .setScale(CONFIG.touchAreaSize * 0.022);
        this.scene.add
            .image(gameSize.width - CONFIG.touchAreaOffset, gameSize.height - CONFIG.touchAreaOffset, "touch-area")
            .setScale(CONFIG.touchAreaSize * 0.022);
    }

    act() {
        this.draw();
        this.update();
    }

    draw() {
        const { graphics } = this;

        graphics.clear();

        this.drawHealthbar();
        this.drawScore();

        if (isMobile) {
            this.drawPointers();
        }
    }

    update() {
        const { scene, backgroundInitialAlpha, backgroundChangeTime } = this;
        const alpha = (1 + Math.sin((backgroundInitialAlpha + scene.gameTime) / backgroundChangeTime)) * 0.25;

        scene.background.setAlpha(alpha);
    }

    drawHealthbar() {
        const { graphics, scene, playerHealthbar } = this;
        const { player } = scene;

        graphics.fillStyle(COLORS.red);
        playerHealthbar.width = HEALTHBAR_WIDTH;
        graphics.fillRectShape(playerHealthbar);

        graphics.fillStyle(COLORS.green);
        playerHealthbar.width = Math.max(HEALTHBAR_WIDTH * (player.hp / player.maxHp), 0);
        graphics.fillRectShape(playerHealthbar);

        graphics.lineStyle(2, COLORS.white);
        playerHealthbar.width = HEALTHBAR_WIDTH;
        graphics.strokeRectShape(playerHealthbar);
    }

    drawScore() {
        const { scene, counterText, scoreText } = this;

        counterText.setText(scene.text);
        scoreText.updateValue(scene.score);
    }

    drawPointers() {
        const { graphics, scene } = this;
        const playerPointers = scene.player.pointers;

        const pointer1 = this.scene.input.pointer1;
        const pointer2 = this.scene.input.pointer2;

        if (pointer1.isDown && (playerPointers.left === pointer1 || playerPointers.right === pointer1)) {
            graphics.strokeCircle(pointer1.x, pointer1.y, CONFIG.pointerSize);
        }

        if (pointer2.isDown && (playerPointers.left === pointer2 || playerPointers.right === pointer2)) {
            graphics.strokeCircle(pointer2.x, pointer2.y, CONFIG.pointerSize);
        }
    }
}
