import { Level } from "levels/Level";
import { levels } from "levels";
import { GameScene } from "scenes/Game";

import { Service } from "./Service";

interface ILevelServiceParams {}

export class LevelService extends Service {
    params: ILevelServiceParams;
    currentLevel: Level;

    constructor(scene: GameScene, params: ILevelServiceParams) {
        super(scene);
        this.params = params;

        this.init();
    }

    init() {
        const { scene } = this;
        const currentLevelNumber = scene.params.level;

        this.currentLevel = new levels[currentLevelNumber](scene);
    }

    act() {
        this.update();
    }

    update() {
        const { scene, currentLevel } = this;

        const isWin = currentLevel.checkForWin();
        scene.text = currentLevel.getTextByMode();
        currentLevel.update();

        if (isWin) {
            scene.onWin();
        }
    }
}
