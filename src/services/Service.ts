import { GameScene } from "scenes/Game";

export abstract class Service {
    scene: GameScene;

    constructor(scene: GameScene) {
        this.scene = scene;
    }

    abstract act(time: number, delta: number): void;
}
