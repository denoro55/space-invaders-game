export const COLORS = {
  white: 0xffffff,
  green: 0x00ff00,
  red: 0xff0000,
  gray: 0xdddddd,
  yellow: 0xfc841f,
};

export const HEX_COLORS = {
  white: "#ffffff",
  green: "#00ff00",
};
