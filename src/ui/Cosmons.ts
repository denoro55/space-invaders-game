import { Vector } from "components";
import { UIComponent } from "./Component";

interface UICosmonsParams {
    pos: Vector;
    value: number;
    isCentered: boolean;
    depth: number;
    prefix?: "+" | "-";
}

const VALUES_OFFSET_X = 12;
const VALUES_OFFSET_Y = 4;

export class UICosmons extends UIComponent {
    params: UICosmonsParams;
    valueText: Phaser.GameObjects.BitmapText;

    constructor(scene: Phaser.Scene, params: UICosmonsParams) {
        super(scene);
        this.params = params;

        this.init();
    }

    init() {
        const { scene, params } = this;
        const { pos, value, isCentered, depth, prefix } = params;
        const valueTextOffset = prefix ? 12 : 0;

        const starImage = scene.add.image(pos.x, pos.y, "coin-gold").setOrigin(0, 0.5).setDepth(depth);
        const prefixBitmapText = new Phaser.GameObjects.BitmapText(
            scene,
            pos.x + starImage.width + VALUES_OFFSET_X - 4,
            pos.y + VALUES_OFFSET_Y + 1,
            "atari",
            prefix,
            22
        )
            .setOrigin(0, 0.5)
            .setDepth(depth);

        if (prefix) {
            scene.add.existing(prefixBitmapText);
        }

        this.valueText = scene.add
            .bitmapText(
                pos.x + starImage.width + VALUES_OFFSET_X + valueTextOffset,
                pos.y + VALUES_OFFSET_Y,
                "atari",
                value.toString(),
                22
            )
            .setOrigin(0, 0.5)
            .setDepth(depth);

        if (isCentered) {
            const halfComponentWidth = (starImage.width + VALUES_OFFSET_X + this.valueText.width + valueTextOffset) / 2;

            starImage.x -= halfComponentWidth;
            this.valueText.x -= halfComponentWidth;
            prefixBitmapText.x -= halfComponentWidth;
        }
    }

    updateValue(value: number) {
        this.valueText.setText(value.toString());
    }
}
