import { COLORS } from "constants/colors";

interface UIButtonParams {
    width: number;
    height: number;
    text: string;
    disabled?: boolean;
}

export class UIButton extends Phaser.GameObjects.Image {
    params: UIButtonParams;
    graphics: Phaser.GameObjects.Graphics;

    constructor(scene: Phaser.Scene, x: number, y: number, texture: string, params: UIButtonParams) {
        super(scene, x, y, texture);

        this.graphics = scene.add.graphics({
            lineStyle: { width: 2, color: COLORS.white },
            fillStyle: { color: COLORS.white },
        });

        this.params = params;

        this.init();
    }

    init() {
        const { scene, graphics, x, y } = this;
        const { width, height, text, disabled } = this.params;

        this.width = width;
        this.height = height;
        this.setOrigin(0.5, 0);

        const alpha = disabled ? 0.5 : 1;

        if (!disabled) {
            this.setInteractive({ cursor: "pointer" });
        } else {
            graphics.alpha = alpha;
        }

        const rect = new Phaser.Geom.Rectangle(x - width / 2, y, width, height);
        graphics.strokeRectShape(rect);

        const buttonText = scene.add
            .bitmapText(x, y + Math.floor(height / 2), "atari", "", 16)
            .setOrigin(0.5)
            .setAlpha(alpha)
            .setCenterAlign();

        buttonText.setText(text);

        this.on("pointerover", () => {
            graphics.alpha = alpha;
            graphics.lineStyle(2, COLORS.green);
            buttonText.tint = COLORS.green;
            document.body.style.cursor = "pointer";
            graphics.strokeRectShape(rect);
        });

        this.on("pointerout", () => {
            graphics.alpha = alpha;
            graphics.lineStyle(2, COLORS.white);
            buttonText.tint = COLORS.white;
            document.body.style.cursor = "default";
            graphics.strokeRectShape(rect);
        });

        this.on("pointerdown", () => {
            document.body.style.cursor = "default";
        });
    }
}
