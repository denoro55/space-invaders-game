import { COLORS } from "constants/colors";
import { Vector } from "components";

import { UIComponent } from "./Component";

interface UIRainbowTextParams {
    text: string;
    pos: Vector;
    fontSize?: number;
    color?: number;
}

const DEFAULT_PARAMS = {
    fontSize: 23,
    color: COLORS.green,
};

export class UIRainbowText extends UIComponent {
    params: UIRainbowTextParams;
    rainbowColorIdx = 0;
    rainbowColorOffset = 0;
    delay = 0;
    rainbowWave = 0;

    constructor(scene: Phaser.Scene, params: UIRainbowTextParams) {
        super(scene);
        this.params = {
            ...DEFAULT_PARAMS,
            ...params,
        };

        this.init();
    }

    init() {
        const { scene } = this;
        const { text, pos, color } = this.params;

        this.rainbowCallback = this.rainbowCallback.bind(this);

        const rainbowText = scene.add
            .dynamicBitmapText(pos.x, pos.y, "atari", text, 23)
            .setOrigin(0.5)
            .setCenterAlign();
        rainbowText.setDisplayCallback(this.rainbowCallback);

        rainbowText.tint = color;
    }

    update() {
        this.updateRainbow();
    }

    updateRainbow() {
        this.rainbowColorIdx = 0;

        if (this.delay++ === 6) {
            this.rainbowColorOffset = (this.rainbowColorOffset + 1) % 4;
            this.delay = 0;
        }
    }

    rainbowCallback(data: Phaser.Types.GameObjects.BitmapText.DisplayCallbackConfig & { color: number }) {
        const { color } = this.params;

        data.color = color;
        this.rainbowColorIdx = (this.rainbowColorIdx + 1) % 4;
        data.y = Math.cos(this.rainbowWave + this.rainbowColorIdx) * 2;
        this.rainbowWave += 0.005;

        return data;
    }
}
