import { isDev, isMobile } from "config";
import { GameScene } from "scenes/Game";
import { MenuScene } from "scenes/Menu";
import { FailScene } from "scenes/Fail";
import { WinScene } from "scenes/Win";
import { PreviewScene } from "scenes/Preview";
import { IGlobalData } from "types";

import { getFullWindowHeight } from "helpers";

const getMobileConfig = () => ({
    width: window.innerWidth,
    height: getFullWindowHeight(),
    // width: 746,
    // height: 414,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
    },
});

const getDesktopConfig = () => ({
    width: 1332,
    height: 768,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
    },
});

export const getGameConfig = () => {
    const deviceOptions = isMobile ? getMobileConfig() : getDesktopConfig();

    return {
        type: Phaser.AUTO,
        parent: 'game-container',
        ...deviceOptions,
        physics: {
            default: "arcade",
            arcade: {
                gravity: { y: 0 },
                debug: false,
            },
        },
        scene: [MenuScene, GameScene, PreviewScene, FailScene, WinScene],
    };
};

export const INITIAL_GAME_DATA: IGlobalData = {
    level: !isDev ? 0 : 0,
    levels: {},
    score: 0,
};
