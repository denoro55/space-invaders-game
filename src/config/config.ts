import { LEVELS_CONFIG } from "config/levels";

export const CONFIG = {
    title: "Space Invaders",
    subtitle: "The battle for the galaxy",
    levelsCount: LEVELS_CONFIG.length,
    failTitleText: "MISSION FAILED",
    failSubtitleText: "Your ship was destroyed.",
    winTitleText: "MISSION COMPLETED",
    pointerSize: 32,
    touchAreaSize: 27,
    touchAreaOffset: 85,
    scene: {
        delta: 16,
        minYPos: 100,
        failTimeoutTime: 2000,
        winTimeoutTime: 2000,
    },
};
