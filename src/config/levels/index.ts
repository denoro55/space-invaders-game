import { Bot, Hunter, UFO } from "objects/enemies";
import { GameMode, ILevelConfig } from "types";

import { getLevelConfig } from "./helpers";

export const LEVELS_CONFIG: ILevelConfig[] = [
    getLevelConfig({
        level: 0,
        title: "Red Alert",
        text: `Space invaders have attacked our base. Come back and find out the reason for the invasion.
    \nWarning! A lot of invaders have been detected on our way.`,
        mode: {
            type: GameMode.totalKills,
            target: 20,
        },
    }),
    getLevelConfig({
        level: 1,
        title: "Hunters",
        text: `Hunters in sight. It's the most common units among the enemy ships. Be careful, they follow for their targets.`,
        mode: {
            type: GameMode.enemyKills,
            target: {
                hunter: 10,
            },
        },
    }),
    getLevelConfig({
        level: 2,
        title: "UFO",
        text: `Seems like aliens are also against us. Or maybe their ships were hijacked?`,
        mode: {
            type: GameMode.time,
            target: 120,
        },
    }),
    getLevelConfig({
        level: 3,
        title: "Defense",
        text: `Ok, we are on the base. Take the hit and help your allies to protect one.`,
        mode: {
            type: GameMode.waves,
            target: [
                {
                    maxEnemies: 4,
                    enemies: [Bot, Bot, Bot, Hunter, Hunter, Bot, Hunter],
                },
                {
                    maxEnemies: 4,
                    enemies: [Hunter, Hunter, Hunter, Bot, Bot, Bot, Bot, Bot, Hunter],
                },
                {
                    maxEnemies: 4,
                    enemies: [
                        Hunter,
                        Hunter,
                        Hunter,
                        Bot,
                        Hunter,
                        Bot,
                        Hunter,
                        Bot,
                        Hunter,
                        Bot,
                        Hunter,
                        Bot,
                        Bot,
                        Bot,
                    ],
                },
                {
                    maxEnemies: 5,
                    enemies: [
                        Bot,
                        Bot,
                        Bot,
                        UFO,
                        Hunter,
                        UFO,
                        Bot,
                        UFO,
                        Hunter,
                        Bot,
                        Hunter,
                        Bot,
                        Bot,
                        UFO,
                        Bot,
                        Hunter,
                        Bot,
                    ],
                },
                {
                    maxEnemies: 5,
                    enemies: [
                        Hunter,
                        Hunter,
                        Hunter,
                        UFO,
                        UFO,
                        Hunter,
                        Hunter,
                        Bot,
                        Bot,
                        Bot,
                        Bot,
                        Bot,
                        Hunter,
                        UFO,
                        Hunter,
                        Bot,
                        UFO,
                        Hunter,
                        Bot,
                        UFO,
                        UFO,
                        Hunter,
                    ],
                },
            ],
        },
    }),
];
