import { ENEMIES_CONFIG } from "config/enemies";
import { LevelMode, EnemyName, GameMode, ILevelConfig } from "types";
import { pluralize } from "helpers";

const getMissionText = (levelMode: LevelMode) => {
    switch (levelMode.type) {
        case GameMode.totalKills: {
            const targetCount = levelMode.target;

            return `Mission: kill ${targetCount} ${pluralize(targetCount, ["invader", "invaders", "invaders"])}`;
        }

        case GameMode.enemyKills: {
            const targets = levelMode.target;

            const text = Object.keys(targets)
                .map((targetName: EnemyName) => {
                    const targetCount = targets[targetName];

                    return `${targetCount} ${pluralize(targetCount, ENEMIES_CONFIG[targetName].pluralName)}`;
                })
                .join(" / ");

            return `Mission: kill ${text}`;
        }

        case GameMode.time: {
            const seconds = levelMode.target;

            return `Mission: stay alive for ${seconds} ${pluralize(seconds, ["second", "seconds", "seconds"])}`;
        }

        case GameMode.waves: {
            const wavesCount = levelMode.target.length;

            return `Mission: Defeat ${wavesCount} waves of enemies`;
        }
    }
};

export const getLevelConfig = (levelConfig: Omit<ILevelConfig, "mission">) => ({
    ...levelConfig,
    mission: getMissionText(levelConfig.mode),
});
