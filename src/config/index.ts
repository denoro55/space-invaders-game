import { isMobileDevice } from "helpers";

export { CONFIG } from "./config";
export { ENEMIES_CONFIG } from "./enemies";
export { LEVELS_CONFIG } from "./levels";
export { getGameConfig } from "./game";

export const isDev = process.env.NODE_ENV === "development";
export const isMobile = isMobileDevice();