export const ENEMIES_CONFIG = {
  bot: {
    pluralName: ["bot", "bots", "bots"],
  },
  hunter: {
    pluralName: ["hunter", "hunters", "hunters"],
  },
  ufo: {
    pluralName: ["ufo", "ufo", "ufo"],
  },
  flyer: {
    pluralName: ["flyer", "flyers", "flyers"],
  },
};
