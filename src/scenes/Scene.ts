import { Game } from "main";
import { IGlobalData } from "types";
import { isDev } from "config";
import { Vector } from "components";

export class Scene extends Phaser.Scene {
    game: Game;

    getData() {
        return this.game.data;
    }

    setData(key: keyof IGlobalData, value: any) {
        this.game.data[key] = value;

        if (isDev) {
            console.log(`[${key}] was updated in global data:`, this.game.data);
        }
    }

    getGameSize() {
        return {
            width: +this.game.config.width,
            height: +this.game.config.height,
        };
    }
}
