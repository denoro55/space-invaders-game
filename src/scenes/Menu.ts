import { COLORS } from "constants/colors";
import { Vector } from "components";
import { CONFIG, LEVELS_CONFIG, isMobile } from "config";
import { UIButton, UICosmons, UIRainbowText } from "ui";
import { SceneName } from "types";

import { Scene } from "./Scene";
import { showFullscreen } from "../helpers";

let globalAssetsLoaded = false;

const FULLSCREEN_TIMEOUT_DELAY = 200;

export class MenuScene extends Scene {
    graphics: Phaser.GameObjects.Graphics;
    rainbowText: UIRainbowText;

    constructor() {
        super({
            key: SceneName.menu,
        });
    }

    preload() {
        this.load.image("coin-gold", "assets/images/bonus/star-gold.png");

        if (!globalAssetsLoaded) {
            this.load.bitmapFont("atari", "assets/fonts/atari/bitmap/gem.png", "assets/fonts/atari/bitmap/gem.xml");
            globalAssetsLoaded = true;
        }
    }

    create() {
        const centerPosX = +this.game.config.width / 2;
        const centerPosY = +this.game.config.height / 2;
        const offsetY = 125;

        this.graphics = this.add.graphics({
            lineStyle: { width: 2, color: COLORS.white },
            fillStyle: { color: COLORS.white },
        });

        this.createTitle(centerPosX, centerPosY - offsetY);
        this.createSubtitle(centerPosX, centerPosY - offsetY + 45);

        new UICosmons(this, {
            pos: new Vector(centerPosX, centerPosY - offsetY + 90),
            value: this.getData().score,
            isCentered: true,
            depth: 100,
        });

        this.createLevelText(centerPosX, centerPosY - offsetY + 150);
        this.createStartButton(centerPosX, centerPosY - offsetY + 185);
        // this.createLabButton(centerPosX, centerPosY + 100 + 65);
        this.createLastUpdatedText(centerPosX, +this.game.config.height - 30);

        // txt = this.add.text(20, 20, '' + rootElement.getBoundingClientRect().height + ':' + this.game.config.height, { font: '"Press Start 2P"' });

        // this.createTitle(CENTER_POS_X, startPosY);
        // this.createSubtitle(CENTER_POS_X, startPosY + 45);

        // new UICosmons(this, {
        //     pos: new Vector(CENTER_POS_X, startPosY + 90),
        //     value: this.getData().score,
        //     isCentered: true,
        //     depth: 100,
        // });

        // this.createLevelText(CENTER_POS_X, startPosY + 90 + 75);
        // this.createStartButton(CENTER_POS_X, startPosY + 200);
        // this.createLabButton(CENTER_POS_X, startPosY + 200 + 65);
        // this.createLastUpdatedText(CENTER_POS_X, CONFIG.height - 30);
    }

    update() {
        this.rainbowText.update();
    }

    createTitle(posX: number, posY: number) {
        this.add.bitmapText(posX, posY, "atari", CONFIG.title, 32).setOrigin(0.5);
    }

    createSubtitle(posX: number, posY: number) {
        this.rainbowText = new UIRainbowText(this, {
            pos: new Vector(posX, posY),
            text: CONFIG.subtitle,
        });
    }

    createLevelText(posX: number, posY: number) {
        const currentLevel = this.getData().level;

        const text = `Next level: ${currentLevel + 1}. ${LEVELS_CONFIG[currentLevel].title}`;

        this.add.dynamicBitmapText(posX, posY, "atari", text, 19).setOrigin(0.5);
    }

    createStartButton(posX: number, posY: number) {
        const startButton = new UIButton(this, posX, posY, "", {
            width: 180,
            height: 46,
            text: "Start game",
        });

        startButton.on("pointerdown", () => {
            this.scene.start(SceneName.preview, {
                level: this.getData().level,
            });

            if (isMobile) {
                setTimeout(() => {
                    showFullscreen(this.game.canvas);
                }, FULLSCREEN_TIMEOUT_DELAY);
            }
        });
    }

    createLabButton(posX: number, posY: number) {
        const labButton = new UIButton(this, posX, posY, "", {
            width: 180,
            height: 46,
            text: "Lab",
            disabled: true,
        });

        labButton.on("pointerdown", () => {
            console.log("lab");
        });
    }

    createLastUpdatedText(posX: number, posY: number) {
        const text = `Last updated: ${new Date(process.env.buildTime).toLocaleString()}`;

        this.add.dynamicBitmapText(posX, posY, "atari", text, 16).setOrigin(0.5);
    }
}
