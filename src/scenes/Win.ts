import { Vector } from "components";
import { CONFIG } from "config";
import { UIButton, UICosmons, UIRainbowText } from "ui";
import { SceneName } from "types";

import { Scene } from "./Scene";

interface WinSceneParams {
    score: number;
    isFlawlessVictory: boolean;
}

interface IScoreParams {
    score: number;
    totalScore: number;
    bestLevelScore: number;
    flawlessVictoryScores: number;
    plusScore: number;
    currentLevel: number;
}

const BUTTON_WIDTH = 160;
const BUTTON_SPACE = 20;

export class WinScene extends Scene {
    graphics: Phaser.GameObjects.Graphics;
    params: WinSceneParams;
    rainbowTitle: UIRainbowText;

    constructor() {
        super({
            key: SceneName.win,
        });
    }

    preload() {
        // code
    }

    init(data: WinSceneParams) {
        this.params = data;
    }

    create() {
        const gameSize = this.getGameSize();

        const centerPosX = gameSize.width / 2;
        let currentPosY = gameSize.height / 2 - 120;

        const globalData = this.getData();
        const { score, isFlawlessVictory } = this.params;
        const bestLevelScore = globalData.levels[globalData.level]?.score || 0;
        const flawlessVictoryScores = isFlawlessVictory ? Math.ceil(score * 0.25) : 0;
        const totalScore = score + flawlessVictoryScores;
        const plusScore = bestLevelScore < totalScore ? totalScore - bestLevelScore : 0;

        const scoreParams = {
            currentLevel: globalData.level,
            score,
            totalScore,
            bestLevelScore,
            flawlessVictoryScores,
            plusScore,
        };

        this.createTitle(centerPosX, currentPosY);
        currentPosY += 35;
        this.createScores(centerPosX, currentPosY, scoreParams);
        currentPosY += 170;
        this.createNextButton(centerPosX + BUTTON_WIDTH + BUTTON_SPACE, currentPosY);
        this.createTryAgainButton(centerPosX - BUTTON_WIDTH - BUTTON_SPACE, currentPosY);
        this.createMenuButton(centerPosX, currentPosY);

        this.updateGlobalData(scoreParams);
    }

    update() {
        this.rainbowTitle.update();
    }

    createTitle(posX: number, posY: number) {
        this.rainbowTitle = new UIRainbowText(this, {
            pos: new Vector(posX, posY),
            text: CONFIG.winTitleText,
            fontSize: 28,
        });
    }

    createScores(posX: number, posY: number, params: IScoreParams) {
        const scoresText = `Score: ${params.score}\nFlawless victory: ${params.flawlessVictoryScores}\nBest score: ${params.bestLevelScore}\nTotal score: ${params.totalScore}`;
        const scoresBitmapText = new Phaser.GameObjects.BitmapText(this, posX, posY, "atari", scoresText, 18)
            .setOrigin(0.5, 0)
            .setCenterAlign();

        this.add.existing(scoresBitmapText);

        new UICosmons(this, {
            pos: new Vector(posX, posY + 120),
            isCentered: true,
            value: params.plusScore,
            prefix: "+",
            depth: 100,
        });
    }

    createNextButton(posX: number, posY: number) {
        const gameSize = this.getGameSize();
        const currentLevel = this.getData().level;

        const nextLevelButton = new UIButton(this, posX, posY, "", {
            width: BUTTON_WIDTH,
            height: 46,
            text: "Next level",
            disabled: CONFIG.levelsCount <= currentLevel + 1,
        });

        nextLevelButton.on("pointerdown", () => {
            this.setData("level", currentLevel + 1);

            const gameParams = {
                level: this.getData().level,
            };

            this.scene.start(SceneName.preview, gameParams);
        });
    }

    createTryAgainButton(posX: number, posY: number) {
        const gameSize = this.getGameSize();
        const tryAgainButton = new UIButton(this, posX, posY, "", {
            width: BUTTON_WIDTH,
            height: 46,
            text: "Try again",
        });

        const gameParams = {
            level: this.getData().level,
        };

        tryAgainButton.on("pointerdown", () => {
            this.scene.start(SceneName.game, gameParams);
        });
    }

    createMenuButton(posX: number, posY: number) {
        const gameSize = this.getGameSize();
        const globalData = this.getData();
        const { level: currentLevel, levels } = globalData;

        const menuButton = new UIButton(this, posX, posY, "", {
            width: BUTTON_WIDTH,
            height: 46,
            text: "Go to menu",
        });

        menuButton.on("pointerdown", () => {
            if (currentLevel + 1 < CONFIG.levelsCount) {
                this.setData("level", currentLevel + 1);
            }

            this.scene.start(SceneName.menu);
        });
    }

    updateGlobalData(params: IScoreParams) {
        const globalData = this.getData();

        if (params.bestLevelScore < params.totalScore) {
            this.setData("levels", {
                ...globalData.levels,
                [params.currentLevel]: {
                    score: params.totalScore,
                },
            });

            this.setData("score", globalData.score + params.plusScore);
        }
    }
}
