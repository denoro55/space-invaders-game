import { COLORS } from "constants/colors";
import { Vector } from "components";
import { CONFIG } from "config";
import { UIButton, UIRainbowText } from "ui";
import { SceneName } from "types";
import { IGameParams } from "scenes/Game/types";

import { Scene } from "./Scene";

interface FailSceneParams {}

const BUTTON_WIDTH = 160;
const BUTTON_SPACE = 20;

export class FailScene extends Scene {
    graphics: Phaser.GameObjects.Graphics;
    params: FailSceneParams;
    rainbowTitle: UIRainbowText;

    constructor() {
        super({
            key: SceneName.fail,
        });
    }

    preload() {
        // code
    }

    init(data: FailSceneParams) {
        this.params = data;
    }

    create() {
        const gameSize = this.getGameSize();

        let currentPosY = gameSize.height / 2 - 60;
        const centerPosX = gameSize.width / 2;

        this.rainbowTitle = new UIRainbowText(this, {
            pos: new Vector(gameSize.width / 2, currentPosY),
            text: CONFIG.failTitleText,
            fontSize: 28,
            color: COLORS.red,
        });

        currentPosY += 20;
        const subtitleText = new Phaser.GameObjects.BitmapText(
            this,
            gameSize.width / 2,
            currentPosY,
            "atari",
            CONFIG.failSubtitleText,
            19
        );

        subtitleText.setOrigin(0.5, 0).setCenterAlign().setTint(COLORS.white);
        this.add.existing(subtitleText);

        currentPosY += 67;
        this.createTryAgainButton(centerPosX - BUTTON_WIDTH / 2 - BUTTON_SPACE / 2, currentPosY);
        this.createMenuButton(centerPosX + BUTTON_WIDTH / 2 + BUTTON_SPACE / 2, currentPosY);
    }

    update() {
        this.rainbowTitle.update();
    }

    createMenuButton(posX: number,posY: number) {
        const menuButton = new UIButton(this, posX, posY, "", {
            width: BUTTON_WIDTH,
            height: 46,
            text: "Go to menu",
        });

        menuButton.on("pointerdown", () => {
            this.scene.start(SceneName.menu);
        });
    }

    createTryAgainButton(posX: number, posY: number) {
        const tryAgainButton = new UIButton(this, posX, posY, "", {
            width: BUTTON_WIDTH,
            height: 46,
            text: "Try again",
        });

        const gameParams: IGameParams = {
            level: this.getData().level,
        };

        tryAgainButton.on("pointerdown", () => {
            this.scene.start(SceneName.game, gameParams);
        });
    }
}
