import { GameScene } from "scenes/Game";
import { Vector } from "components";
import { Coin } from "objects/bonuses/Coin";
import { angleToRadians } from "helpers";
import { Explode } from "objects/effects";

export class Utils {
  scene: GameScene;

  constructor(scene: GameScene) {
    this.scene = scene;
  }

  explode(x: number, y: number) {
    const { scene } = this;

    scene.effects.push(
      new Explode(scene, new Vector(x, y), {
        sprite: "orange-explode",
      })
    );
  }

  tripleExplode(x: number, y: number) {
    const fullCircleRange = 360;
    const range = 30;
    const initialAngle = (Math.random() * fullCircleRange) / 3;

    for (let i = 0; i < 3; i++) {
      const angle = angleToRadians(i * (fullCircleRange / 3) + initialAngle);
      this.explode(x + Math.cos(angle) * range, y + Math.sin(angle) * range);
    }
  }

  createBonus(x: number, y: number) {
    const { scene } = this;

    scene.bonuses.push(new Coin(scene, new Vector(x, y)));
  }
}
