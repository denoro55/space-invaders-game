export function preload() {
  // this.load.image("background", "assets/images/background/black-tile.png");
  this.load.image("background", "assets/images/background/gray.jpg");

  this.load.image("player-ship", "assets/images/ships/player-ship.png");
  this.load.image("enemy-bot-ship", "assets/images/ships/enemy-ship-2.png");
  this.load.image("enemy-hunter-ship", "assets/images/ships/enemy-hunter.png");
  this.load.image("enemy-ufo", "assets/images/ships/enemy-ufo.png");
  this.load.image("enemy-flyer", "assets/images/ships/enemy-flyer.png");

  this.load.image("laser-green", "assets/images/bullets/laser-green-3.png");
  this.load.image("laser-red", "assets/images/bullets/laser-red-2.png");
  this.load.image("sphere-bullet", "assets/images/bullets/sphere-bullet.png");

  this.load.image("green-explode", "assets/images/effects/green-explode.png");
  this.load.image("red-explode", "assets/images/effects/red-explode.png");
  this.load.image("orange-explode", "assets/images/effects/orange-explode.png");

  this.load.image("star", "assets/images/effects/star-4.png");
  this.load.image("fire", "assets/images/effects/fire-3.png");
  this.load.image("comet", "assets/images/effects/comet.png");
  this.load.image("meteor-1", "assets/images/effects/meteor-gray-1.png");
  this.load.image("meteor-2", "assets/images/effects/meteor-gray-2.png");
  this.load.image("meteor-3", "assets/images/effects/meteor-gray-3.png");
  this.load.image("meteor-4", "assets/images/effects/meteor-gray-4.png");

  // audio
  this.load.audio("laser-1", ["assets/audio/sfx_laser1.ogg"]);
  this.load.audio("laser-2", ["assets/audio/sfx_laser2.ogg"]);

  this.load.image("touch-area", "assets/images/controls/touch-area.png");

  // bonus
  this.load.image("coin-bronze", "assets/images/bonus/star-bronze.png");
  this.load.image("coin-silver", "assets/images/bonus/star-silver.png");
}
