import { EnemyName } from "types";

export interface IStats {
    totalKills: number;
    kills: Record<EnemyName, number>;
}

export interface IGameParams {
    level: number;
}
