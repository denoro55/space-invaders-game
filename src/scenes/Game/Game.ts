import { CONFIG, isMobile } from "config";
import {
    Service,
    MeteorsService,
    StarsService,
    LoggerService,
    InterfaceService,
    LevelService,
    CometsService,
} from "services";
import { Scene } from "scenes/Scene";
import { Player } from "objects/Player";
import { Enemy } from "objects/enemies";
import { Bullet } from "objects/bullets";
import { Effect } from "objects/effects";
import { Bonus } from "objects/bonuses";
import { SceneName } from "types";

import { preload } from "./preload";
import { Utils } from "./Utils";

import { getInitialGameData } from "./helpers";
import { IStats, IGameParams } from "./types";

let graphics: Phaser.GameObjects.Graphics;
let firstInited = false;

export class GameScene extends Scene {
    public player: Player;
    public utils: Utils;

    public enemies: Enemy[] = [];
    public bullets: Bullet[] = [];
    public effects: Effect[] = [];
    public bonuses: Bonus[] = [];

    public text: string; // top right
    public score: number = 0;

    private services: Service[] = [];

    public params: IGameParams;
    public stats: IStats = getInitialGameData();

    public isWon: boolean = false;
    public isFail: boolean = false;

    public gameTime: number = 0;

    public background: Phaser.GameObjects.Image;
    public shakeCamera: Phaser.Cameras.Scene2D.Camera;

    constructor() {
        super({
            key: SceneName.game,
        });
    }

    preload() {}

    init(params: IGameParams) {
        this.params = params;
    }

    create() {
        this.resetState();

        const gameSize = this.getGameSize();

        this.physics.world.setBounds(0, 0, gameSize.width, gameSize.height);
        // this.add.tileSprite(CONFIG.width / 2, CONFIG.height / 2, CONFIG.width, CONFIG.height, "background");
        this.background = this.add.image(0, 0, "background").setOrigin(0, 0).setAlpha(0.2);

        graphics = this.add.graphics({
            lineStyle: { width: 2, color: 0x00ff00 },
            fillStyle: { color: 0xff0000 },
        });

        this.player = new Player(this);
        this.utils = new Utils(this);

        this.initListeners();
        this.initServices();
        this.initTimer();

        if (isMobile) {
            this.initPointers();
        }

        this.shakeCamera = this.cameras.add(0, 0, gameSize.width, gameSize.height);

        firstInited = true;
    }

    initPointers() {
        const gameSize = this.getGameSize();

        if (!firstInited) {
            this.input.addPointer(1);
        }

        this.input.on(
            "pointerdown",
            (pointer: Phaser.Input.Pointer) => {
                const touchLeftCircleArea = new Phaser.Geom.Circle(
                    CONFIG.touchAreaOffset,
                    gameSize.height - CONFIG.touchAreaOffset,
                    CONFIG.touchAreaSize
                );
                const touchRightCircleArea = new Phaser.Geom.Circle(
                    gameSize.width - CONFIG.touchAreaOffset,
                    gameSize.height - CONFIG.touchAreaOffset,
                    CONFIG.touchAreaSize
                );

                const pointerCircleArea = new Phaser.Geom.Circle(pointer.x, pointer.y, CONFIG.pointerSize);

                if (Phaser.Geom.Intersects.CircleToCircle(touchLeftCircleArea, pointerCircleArea)) {
                    this.player.setPointer("left", pointer);
                }

                if (Phaser.Geom.Intersects.CircleToCircle(touchRightCircleArea, pointerCircleArea)) {
                    this.player.setPointer("right", pointer);
                }
            },
            this
        );

        this.input.on(
            "pointerup",
            (pointer: Phaser.Input.Pointer) => {
                if (this.player.pointers.left === pointer) {
                    this.player.pointers.left = null;
                }

                if (this.player.pointers.right === pointer) {
                    this.player.pointers.right = null;
                }
            },
            this
        );
    }

    initTimer() {
        // this.time.addEvent({ delay: 10, callback: this.onClockEvent, callbackScope: this, loop: true });
    }

    onClockEvent() {
        // this.gameTime += 15;
    }

    initListeners() {
        const keyM = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.M);

        keyM.on("down", () => {
            if (this.player.body.active) {
                this.scene.start(SceneName.menu);
            }
        });
    }

    initServices() {
        // P.S interesting generic type detection
        this.addService(StarsService, { count: 20 }); // ok 200ms
        this.addService(MeteorsService, {}); // ok 300ms
        // this.addService(LoggerService, {}); // ok 170ms
        this.addService(InterfaceService, {});
        this.addService(LevelService, {});
        this.addService(CometsService, {});
    }

    addService<T>(ServiceConstructor: new (scene: GameScene, params: T) => Service, params: T) {
        this.services.push(new ServiceConstructor(this, params));
    }

    update(time: number, delta: number) {
        graphics.clear();

        this.gameTime += 15 * (delta / CONFIG.scene.delta);

        this.enemies.forEach((enemy) => {
            enemy.act(time, delta);
        });

        this.bullets.forEach((bullet) => {
            bullet.act(time, delta);
        });

        this.effects.forEach((effect) => {
            effect.act();
        });

        this.services.forEach((service) => {
            service.act(time, delta);
        });

        this.bonuses.forEach((bonus) => {
            bonus.act();
        });

        this.player.act(time, delta);
    }

    resetState() {
        this.bullets = [];
        this.enemies = [];
        this.effects = [];
        this.bonuses = [];

        this.score = 0;
        this.text = "";

        this.services = [];
        this.stats = getInitialGameData();

        this.isWon = false;
        this.isFail = false;

        this.gameTime = 0;
    }

    onFail() {
        this.isFail = true;

        setTimeout(() => {
            this.scene.start(SceneName.fail);
        }, CONFIG.scene.failTimeoutTime);
    }

    onWin() {
        if (!this.isWon && !this.isFail) {
            this.isWon = true;
            setTimeout(() => {
                if (!this.isFail) {
                    this.scene.start(SceneName.win, {
                        score: this.score,
                        isFlawlessVictory: this.player.hp === this.player.maxHp,
                    });
                }
            }, CONFIG.scene.winTimeoutTime);
        }
    }

    shakeScreen() {
        this.shakeCamera.shake(500, 0.003);
    }

    getStats() {
        return this.stats;
    }
}

GameScene.prototype.preload = preload;
