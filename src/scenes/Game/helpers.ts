export const getInitialGameData = () => ({
  totalKills: 0,
  kills: {
    bot: 0,
    hunter: 0,
    flyer: 0,
    ufo: 0,
  },
});
