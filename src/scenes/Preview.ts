import { COLORS } from "constants/colors";
import { CONFIG, LEVELS_CONFIG } from "config";
import { UIButton } from "ui";
import { IGameParams } from "scenes/Game/types";
import { SceneName } from "types";

import { Scene } from "./Scene";

interface PreviewSceneParams {
    level: number;
}

export class PreviewScene extends Scene {
    graphics: Phaser.GameObjects.Graphics;
    params: PreviewSceneParams;

    constructor() {
        super({
            key: SceneName.preview,
        });
    }

    init(data: PreviewSceneParams) {
        this.params = data;
    }

    create() {
        const { params } = this;
        const { level } = params;
        const { title, text, mission } = LEVELS_CONFIG[level];

        const gameSize = this.getGameSize();

        const centerY = gameSize.height / 2;

        const titleText = new Phaser.GameObjects.BitmapText(
            this,
            gameSize.width / 2,
            0,
            "atari",
            `${level + 1}. ${title}`,
            32
        );
        titleText.setOrigin(0.5, 0).setCenterAlign();

        const descriptionText = new Phaser.GameObjects.BitmapText(this, gameSize.width / 2, 0, "atari", text, 18);
        descriptionText.setMaxWidth(450).setOrigin(0.5, 0).setCenterAlign();

        const missionText = new Phaser.GameObjects.BitmapText(this, gameSize.width / 2, 0, "atari", mission, 18);
        missionText.setMaxWidth(450).setOrigin(0.5, 0).setCenterAlign().setTint(COLORS.green);

        const totalContentHeight = titleText.height + descriptionText.height + missionText.height + 100;
        const startY = centerY - totalContentHeight / 2 + 5;

        titleText.y = startY - 25;
        descriptionText.y = titleText.y + 65;
        missionText.y = descriptionText.y + descriptionText.height + 25;

        this.add.existing(titleText);
        this.add.existing(descriptionText);
        this.add.existing(missionText);

        this.drawButton(missionText.y + missionText.height + 40);
    }

    drawButton(posY: number) {
        const gameSize = this.getGameSize();

        const goButton = new UIButton(this, gameSize.width / 2, posY, "", {
            width: 120,
            height: 46,
            text: "Go",
        });

        const gameParams: IGameParams = {
            level: this.params.level,
        };

        goButton.on("pointerdown", () => {
            this.scene.start(SceneName.game, gameParams);
        });
    }
}
