import { GameScene } from "scenes/Game";

import { Level } from "./Level";

export class Level4 extends Level {
    nextAttemptTimeInterval: number = 1500;

    constructor(scene: GameScene) {
        super(scene, 3);
    }

    update() {
        super.update();
    }
}
