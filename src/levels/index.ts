import { Level1 } from "./Level1";
import { Level2 } from "./Level2";
import { Level3 } from "./Level3";
import { Level4 } from "./Level4";

export const levels = [Level1, Level2, Level3, Level4];

/*
1. 3-5 easy
2. 2-4 easy 0-2 medium
3. 2-4 easy 2+ medium
4. 
*/