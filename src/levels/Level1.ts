import { TotalKillsMode } from "types/level";
import { Bot } from "objects/enemies";
import { GameScene } from "scenes/Game";

import { Level } from "./Level";

const MAX_ENEMIES_COUNT = 5;
const CHANCE_TO_MAX_COUNT = 0.8;
const COUNT_FOR_INCREMENT_MAX = 0.66;

export class Level1 extends Level {
    nextAttemptTimeInterval: number = 1500;

    constructor(scene: GameScene) {
        super(scene, 0);
    }

    makeAttempt() {
        const { scene } = this;
        const { enemies } = scene;

        const stats = scene.getStats();
        const mode = this.mode as TotalKillsMode;

        const maxCount =
            stats.totalKills > Math.ceil(mode.target * COUNT_FOR_INCREMENT_MAX)
                ? MAX_ENEMIES_COUNT
                : MAX_ENEMIES_COUNT - 1;

        if (enemies.length < MAX_ENEMIES_COUNT - 2) {
            this.createEnemy(Bot);
        } else if (enemies.length < maxCount) {
            if (Math.random() > CHANCE_TO_MAX_COUNT) {
                this.createEnemy(Bot);
            }
        }
    }
}
