import { Bot, Hunter, UFO } from "objects/enemies";
import { GameScene } from "scenes/Game";
import { EnemyName } from "types";

import { Level } from "./Level";

const MAX_ENEMIES_COUNT = 3;

export class Level3 extends Level {
    nextAttemptTimeInterval: number = 1200;

    additionalTime: number = 0;
    additionalTimeInterval: number = 5000;

    constructor(scene: GameScene) {
        super(scene, 2);

        this.additionalTime = scene.gameTime + this.additionalTimeInterval;
    }

    update() {
        const { scene, additionalTime, additionalTimeInterval } = this;
        const time = scene.gameTime;

        if (time > additionalTime) {
            this.spawnEnemy(true);
            this.additionalTime = scene.gameTime + additionalTimeInterval;
        }

        super.update();
    }

    makeAttempt() {
        const { scene } = this;
        const { enemies } = scene;

        if (enemies.length < MAX_ENEMIES_COUNT) {
            this.spawnEnemy();
        }
    }

    spawnEnemy(isAdditionalSpawn = false) {
        const { scene } = this;
        const rand = Math.random();

        const huntersAliveCount = scene.enemies.filter((e) => e.name === EnemyName.hunter).length;
        const ufoAliveCount = scene.enemies.filter((e) => e.name === EnemyName.ufo).length;
        const totalHeavyEnemiesCount = huntersAliveCount + ufoAliveCount;

        if (rand > 0.7 && (totalHeavyEnemiesCount < 2 || isAdditionalSpawn)) {
            this.createEnemy(UFO);
        } else if (rand > 0.3 && (totalHeavyEnemiesCount < 2 || isAdditionalSpawn)) {
            this.createEnemy(Hunter);
        } else {
            this.createEnemy(Bot);
        }
    }
}
