import { Bot, Hunter } from "objects/enemies";
import { GameScene } from "scenes/Game";
import { EnemyName } from "types";
import { EnemyKillsMode } from "types/level";

import { Level } from "./Level";

const MAX_ENEMIES_COUNT = 4;

export class Level2 extends Level {
    nextAttemptTimeInterval: number = 1500;

    constructor(scene: GameScene) {
        super(scene, 1);
    }

    makeAttempt() {
        const { scene } = this;
        const { enemies } = scene;
        const stats = scene.getStats();
        const mode = this.mode as EnemyKillsMode;

        const chanceToMax = stats.kills.hunter > Math.ceil(mode.target.hunter / 2) ? 0.4 : 0;

        if (enemies.length < MAX_ENEMIES_COUNT - 1) {
            this.spawnEnemy();
        } else if (enemies.length < MAX_ENEMIES_COUNT) {
            if (Math.random() > 0.75 - chanceToMax) {
                this.spawnEnemy();
            }
        }
    }

    spawnEnemy() {
        const { scene } = this;
        const stats = scene.getStats();

        const huntersAlive = scene.enemies.filter((e) => e.name === EnemyName.hunter).length;
        const isHunter = Math.floor(stats.kills.bot / 3) > stats.kills.hunter + huntersAlive;

        if ((Math.random() > 0.3 && !isHunter) || huntersAlive >= 2) {
            this.createEnemy(Bot);
        } else {
            this.createEnemy(Hunter);
        }
    }
}
