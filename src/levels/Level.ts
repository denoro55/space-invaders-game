import { LevelMode } from "types/level";
import { Enemy } from "objects/enemies/Enemy";
import { GameScene } from "scenes/Game";
import { EnemyName, GameMode } from "types";
import { CONFIG } from "config";
import { LEVELS_CONFIG } from "config/levels";
import { Vector } from "components";
import { ILevelState } from "types/level";

export abstract class Level {
    protected scene: GameScene;

    protected nextAttemptTime: number = 0;
    protected abstract nextAttemptTimeInterval: number;

    private status: number;

    protected mode: LevelMode;
    private levelState: ILevelState | null = null;

    constructor(scene: GameScene, levelNumber: number) {
        this.scene = scene;
        this.mode = LEVELS_CONFIG[levelNumber].mode;

        if (this.mode.type === GameMode.waves) {
            this.setLevelState(0);
        }
    }

    setLevelState(levelNumber: number) {
        const { mode } = this;

        if (mode.type === GameMode.waves) {
            const maxMaves = mode.target.length;

            if (levelNumber >= maxMaves) {
                this.levelState = {
                    waveNumber: levelNumber,
                    enemies: [],
                    maxEnemies: 0,
                };
            } else {
                this.levelState = {
                    waveNumber: levelNumber,
                    enemies: [...mode.target[levelNumber].enemies],
                    maxEnemies: mode.target[levelNumber].maxEnemies,
                };
            }
        }
    }

    update() {
        const { scene, nextAttemptTime, nextAttemptTimeInterval } = this;
        const time = scene.gameTime;

        if (time > nextAttemptTime) {
            this.makeAttempt();
            this.nextAttemptTime = time + nextAttemptTimeInterval;
        }

        this.updateStatus();
    }

    updateStatus() {
        const { scene, mode } = this;
        const stats = scene.getStats();

        const modeType = mode.type;

        switch (mode.type) {
            case GameMode.totalKills: {
                const targetKills = +mode.target;

                this.status = targetKills - stats.totalKills;

                break;
            }

            case GameMode.enemyKills: {
                const targets = mode.target;
                const targetsCount = Object.values(targets).reduce((acc, e) => acc + e, 0);
                const targetsKilled = Object.keys(targets).reduce((acc, e: EnemyName) => {
                    const maxKills = (targets as Record<EnemyName, number>)[e];
                    return acc + Math.min(maxKills, stats.kills[e] || 0);
                }, 0);

                this.status = targetsCount - targetsKilled;

                break;
            }

            case GameMode.time: {
                this.status = Math.ceil(+mode.target - scene.gameTime / 1000);

                break;
            }

            case GameMode.waves: {
                const maxWaves = mode.target.length;

                this.status = maxWaves - this.levelState.waveNumber;

                break;
            }

            default: {
                throw new Error(`Mode "${modeType}" is not set or not defined!`);
            }
        }

        this.status = Math.max(this.status, 0);
    }

    getTextByMode(): string {
        if (this.status === undefined) return "";

        return this.status.toString();
    }

    checkForWin() {
        const { status } = this;

        if (status === undefined) return false;

        return status <= 0;
    }

    createEnemy(EnemyConstructor: new (scene: GameScene, pos: Vector) => Enemy) {
        const { scene } = this;

        const gameSize = this.scene.getGameSize();

        const mapping = {
            x: gameSize.width,
            y: gameSize.height,
        };

        const axis = Math.random() > 0.5 ? "y" : "x";
        const points = [-50, mapping[axis] + 50];
        const pointIndex = Math.floor(Math.random() * 2);
        const resultPoint = points[pointIndex];

        const spawnPoint = {
            x: axis === "x" ? resultPoint : Math.random() * gameSize.width,
            y: axis === "y" ? resultPoint : Math.random() * gameSize.height,
        };

        scene.enemies.push(new EnemyConstructor(scene, spawnPoint));
    }

    makeAttempt() {
        const { scene, mode, levelState } = this;

        if (mode.type === GameMode.waves) {
            const { maxEnemies, enemies } = levelState;
            const maxMaves = mode.target.length;
            const enemiesCount = scene.enemies.length;

            if (enemiesCount < maxEnemies) {
                const nextEnemy = enemies.shift();

                if (nextEnemy) {
                    this.createEnemy(nextEnemy);
                } else if (enemiesCount <= 0 && levelState.waveNumber < maxMaves) {
                    this.setLevelState(levelState.waveNumber + 1);
                }
            }
        }
    }
}
