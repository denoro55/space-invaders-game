import { Vector } from "components";
import { angleToDegrees } from "helpers/angle";
import { GameScene } from "scenes/Game";
import { Effect } from "./Effect";

interface IFireParams {
    angle: number;
}

export class Fire extends Effect {
    params: IFireParams;
    private startTime: number;
    private speed: number = 1.7;

    constructor(scene: GameScene, pos: Vector, params: IFireParams) {
        super(scene);
        this.body = new Phaser.GameObjects.Sprite(scene, pos.x, pos.y, "fire");
        this.params = params;
        this.body.setDepth(5);
        this.body.setAngle(angleToDegrees(params.angle) + 90);
        this.startTime = scene.gameTime;
        this.params = params;

        scene.add.existing(this.body);
    }

    act() {
        this.update();
    }

    update() {
        const { scene, body, speed, startTime, params } = this;

        const timeDiff = scene.gameTime - startTime;

        body.alpha = 1 - timeDiff / 150;

        body.x += speed * Math.cos(params.angle);
        body.y += speed * Math.sin(params.angle);

        if (body.alpha <= 0) {
            this.destroy();
        }
    }
}
