import { GameScene } from "scenes/Game";

export class Effect {
    scene: GameScene;
    body: Phaser.GameObjects.Sprite;

    constructor(scene: GameScene) {
        this.scene = scene;
    }

    act() {}

    destroy() {
        const { body } = this;

        this.scene.effects = this.scene.effects.filter((b) => b !== this);
        body.destroy();
    }
}
