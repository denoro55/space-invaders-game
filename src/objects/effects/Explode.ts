import { Vector } from "components";
import { GameScene } from "scenes/Game";

import { Effect } from "./Effect";

interface IExplodeParams {
    sprite: string;
}

export class Explode extends Effect {
    params: IExplodeParams;
    startTime: number;

    constructor(scene: GameScene, pos: Vector, params: IExplodeParams) {
        super(scene);
        this.body = new Phaser.GameObjects.Sprite(scene, pos.x, pos.y, params.sprite);
        this.params = params;
        this.startTime = scene.gameTime;
        this.body.setScale(0);
        this.body.setDepth(115);

        scene.add.existing(this.body);
    }

    act() {
        this.update();
    }

    update() {
        const { scene, startTime } = this;
        const diff = scene.gameTime - startTime;

        const coef = Math.sin(diff / 150);
        const scale = coef;

        this.body.angle += 3;
        this.body.setScale(scale, scale);
        this.body.setAlpha(coef);

        if (coef < 0.06 && diff > 100) {
            this.destroy();
        }
    }
}
