import { Vector } from "components";
import { GameScene } from "scenes/Game";

import { Effect } from "./Effect";

interface IShadowParams {
    sprite: string;
    lifeTime: number;
    scale: number;
    angle: number;
    alpha: number;
}

export class Shadow extends Effect {
    params: IShadowParams;
    destroyTime: number;

    constructor(scene: GameScene, pos: Vector, params: IShadowParams) {
        super(scene);
        this.params = params;

        this.body = new Phaser.GameObjects.Sprite(scene, pos.x, pos.y, params.sprite);
        this.body.setScale(params.scale);
        this.body.setAngle(params.angle);
        this.body.setAlpha(params.alpha);
        this.body.setDepth(0);

        this.destroyTime = params.lifeTime + scene.gameTime;

        scene.add.existing(this.body);
    }

    act() {
        this.update();
    }

    update() {
        const { scene, body, destroyTime, params } = this;
        const { gameTime } = scene;

        const alpha = ((destroyTime - gameTime) * params.alpha) / params.lifeTime;
        body.setAlpha(alpha);

        if (gameTime > this.destroyTime) {
            this.destroy();
        }
    }
}
