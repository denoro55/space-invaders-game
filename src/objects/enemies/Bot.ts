import { CONFIG } from "config";
import { Vector } from "components";
import {
    getAngleBetween,
    angleToDegrees,
    getNearestAngle,
    angleToRadians,
    getOffsetToAngle,
    getCenterBody,
} from "helpers";
import { GameScene } from "scenes/Game";
import { Owner, EnemyName } from "types";

import { Enemy } from "./Enemy";
import { SphereBullet } from "./../bullets";

interface IBotParams {
    x: number;
    y: number;
}

export class Bot extends Enemy {
    public body: Phaser.Types.Physics.Arcade.ImageWithDynamicBody;

    // movement
    private speed: number = 1.5;
    private rotateSpeed: number = 2;
    private lastChangeTargetTime: number = 0;
    private changeTargetInterval: number = 1000;
    private changeTargetIntervalRange: number = 2000;
    private targetAngle: number;

    // attack
    public weaponDamage: number = 10;
    private lastShootTime: number = 0;
    private shootInterval: number = 1200;
    private shootIntervalRange: number = 200;
    private bulletSpeed: number = 7;

    // base
    protected hp: number = 30;
    private bodySize: number = 32;
    private depth: number = 50;
    private scale: number = 0.6;
    protected coinChance: number = 0.9;

    // private lastFireTime: number = 0;
    // private fireReloadTime: number = 100;
    // private laserSound: Phaser.Sound.BaseSound;

    constructor(scene: GameScene, params: IBotParams) {
        super(scene, EnemyName.bot);
        this.body = scene.physics.add.image(params.x, params.y, "enemy-bot-ship");
        // this.laserSound = scene.sound.add("laser-1");

        this.init();

        this.defineTarget = this.defineTarget.bind(this);
        this.defineTarget();
    }

    init() {
        const { scale, depth, body, bodySize } = this;

        body.setScale(scale);
        body.setImmovable(true);
        body.setDepth(depth);

        const speed = this.speed + Math.random();

        this.speed = speed;
        this.rotateSpeed = speed;

        const centerPos = getCenterBody(body.displayWidth, body.displayHeight, scale, bodySize);

        body.setCircle(bodySize, centerPos.x, centerPos.y);
    }

    act(time: number, delta: number) {
        this.update(time, delta);
    }

    update(time: number, delta: number) {
        const { scene, lastShootTime } = this;
        const gameTime = scene.gameTime;

        this.updateWorldCollisions(time, delta);

        this.updateBase(time, delta);

        if (gameTime > this.lastChangeTargetTime) {
            this.updateTarget();
        }

        if (gameTime > lastShootTime) {
            const playerBody = scene.player.body;

            this.shoot(new Vector(playerBody.x, playerBody.y));
        }
    }

    updateBase(time: number, delta: number) {
        const { body, angle, targetAngle, rotateSpeed } = this;

        this.body.angle = angle - 90;

        const angleInRadians = angleToRadians(angle);
        const deltaSpeed = delta / CONFIG.scene.delta;

        body.x += this.speed * Math.cos(angleInRadians) * deltaSpeed;
        body.y += this.speed * Math.sin(angleInRadians) * deltaSpeed;

        this.angle = getNearestAngle(angle, targetAngle, rotateSpeed);
    }

    updateTarget() {
        const { scene, changeTargetInterval, changeTargetIntervalRange } = this;
        const time = scene.gameTime;

        this.defineTarget();

        const intervalTime = changeTargetInterval + Math.random() * changeTargetIntervalRange;
        this.lastChangeTargetTime = time + intervalTime;
    }

    defineTarget() {
        const gameSize = this.scene.getGameSize();

        const target = new Vector(Math.random() * gameSize.width, Math.random() * gameSize.height);

        this.setTarget(target);
    }

    setTarget(target: Vector) {
        const { body } = this;

        this.targetAngle = angleToDegrees(getAngleBetween(target, body));
    }

    shoot(target: Vector) {
        const { body, bulletSpeed, shootInterval, shootIntervalRange, weaponDamage } = this;

        // this.laserSound.play();
        const rotation = getAngleBetween(target, new Vector(body.x, body.y));

        this.scene.bullets.push(
            new SphereBullet(this.scene, new Vector(body.x, body.y), Owner.enemy, {
                speed: getOffsetToAngle(rotation, bulletSpeed),
                angle: rotation,
                sprite: "laser-red",
                explodeSprite: "red-explode",
                damage: weaponDamage,
            })
        );

        this.lastShootTime = this.scene.gameTime + shootInterval + Math.random() * shootIntervalRange;
    }

    onOutOfWorld(nextTarget: Vector) {
        this.setTarget(nextTarget);
    }

    destroy() {
        const { scene, body } = this;

        scene.utils.tripleExplode(body.x, body.y);

        super.destroy();
    }

    // updateFire() {
    //   const { scene, body, angle, lastFireTime, fireReloadTime } = this;
    //   const time = scene.gameTime;

    //   const invertedAngle = angleToRadians(angle) + Math.PI;
    //   const fireDistance = 25;
    //   const posOffset = 0.4;

    //   if (time > lastFireTime) {
    //     scene.effects.push(
    //       new Fire(
    //         scene,
    //         new Vector(
    //           body.x + Math.cos(invertedAngle + posOffset) * fireDistance,
    //           body.y + Math.sin(invertedAngle + posOffset) * fireDistance
    //         ),
    //         {
    //           angle: invertedAngle,
    //         }
    //       )
    //     );
    //     scene.effects.push(
    //       new Fire(
    //         scene,
    //         new Vector(
    //           body.x + Math.cos(invertedAngle - posOffset) * fireDistance,
    //           body.y + Math.sin(invertedAngle - posOffset) * fireDistance
    //         ),
    //         {
    //           angle: invertedAngle,
    //         }
    //       )
    //     );
    //     this.lastFireTime = time + fireReloadTime;
    //   }
    // }
}
