import { CONFIG } from "config";
import { Vector } from "components";
import {
    getAngleBetween,
    angleToDegrees,
    getNearestAngle,
    angleToRadians,
    getOffsetToAngle,
    getCenterBody,
    getAngleDiff,
} from "helpers";
import { Shadow } from "objects/effects";
import { GameScene } from "scenes/Game";
import { Owner, EnemyName } from "types";

import { Enemy } from "./Enemy";
import { SphereBullet } from "./../bullets";

interface IBotParams {
    x: number;
    y: number;
}

export class Flyer extends Enemy {
    public body: Phaser.Types.Physics.Arcade.ImageWithDynamicBody;

    // move
    private maxSpeed: number = 4;
    private minSpeed: number = 1;
    private speed: number = 2;
    private speedChange: number = 0.1;
    private rotateSpeed: number = 2;
    private targetAngle: number;

    // attack
    private bulletSpeed: number = 7;
    private lastShootTime: number = 0;
    private shootTime: number = 1200;
    private shootTimeInterval: number = 200;
    public weaponDamage: number = 10;

    // shadow
    private lastShadowTime: number = 0;
    private shadowTime: number = 50;

    // base
    protected hp: number = 50;
    protected coinChance: number = 0.9;
    private bodySize: number = 32;
    private depth: number = 50;
    private scale: number = 0.6;

    constructor(scene: GameScene, params: IBotParams) {
        super(scene, EnemyName.flyer);
        this.body = scene.physics.add.image(params.x, params.y, "enemy-flyer");

        this.init();

        this.defineRandomTargetPoint = this.defineRandomTargetPoint.bind(this);
        this.defineRandomTargetPoint();
    }

    init() {
        const { scale, depth, body, bodySize } = this;

        body.setScale(scale);
        body.setImmovable(true);
        body.setDepth(depth);

        const centerPos = getCenterBody(body.displayWidth, body.displayHeight, scale, bodySize);

        body.setCircle(bodySize, centerPos.x, centerPos.y);
    }

    act(time: number, delta: number) {
        this.update(time, delta);
    }

    update(time: number, delta: number) {
        this.updateWorldCollisions(time, delta);

        this.updateBase(time, delta);
        this.updateMovement(time, delta);
        this.updateAttack(time, delta);
        this.updateShadows();
    }

    updateBase(time: number, delta: number) {
        const { body, angle, targetAngle, rotateSpeed } = this;

        this.body.angle = angle - 90;

        const angleInRadians = angleToRadians(angle);
        const deltaSpeed = delta / CONFIG.scene.delta;

        body.x += this.speed * Math.cos(angleInRadians) * deltaSpeed;
        body.y += this.speed * Math.sin(angleInRadians) * deltaSpeed;

        this.angle = getNearestAngle(angle, targetAngle, rotateSpeed);
    }

    updateMovement(time: number, delta: number) {
        const { speedChange, angle, targetAngle } = this;

        if (getAngleDiff(angle, targetAngle) < 3) {
            if (this.speed < this.maxSpeed) {
                this.speed += speedChange;
            }
        } else {
            if (this.speed > this.minSpeed) {
                this.speed -= speedChange;
            }
        }
    }

    updateAttack(time: number, delta: number) {
        const { scene, body, shootTime, shootTimeInterval } = this;
        const gameTime = scene.gameTime;

        if (gameTime > this.lastShootTime) {
            const playerBody = scene.player.body;

            const playerPos = new Vector(playerBody.x, playerBody.y);
            const angleToPlayer = getAngleBetween(playerPos, new Vector(body.x, body.y));

            if (Math.random() > 0.1) {
                const spreadRange = 0.25 + Math.random() * 0.3;
                this.shoot(angleToPlayer + spreadRange);
                this.shoot(angleToPlayer - spreadRange);
            } else {
                this.shoot(angleToPlayer);
            }

            this.lastShootTime = gameTime + shootTime + Math.random() * shootTimeInterval;
        }
    }

    updateShadows() {
        const { scene, lastShadowTime, shadowTime } = this;
        const gameTime = scene.gameTime;

        if (gameTime > lastShadowTime) {
            this.lastShadowTime = gameTime + shadowTime;

            this.createShadow();
        }
    }

    createShadow() {
        const { scene, body, scale, angle } = this;
        const speedTime = Math.max(this.maxSpeed - this.speed, 0) * 0.25;

        scene.effects.push(
            new Shadow(scene, new Vector(body.x, body.y), {
                sprite: "enemy-flyer",
                lifeTime: 250,
                scale,
                angle: angle - 90,
                alpha: 0.4 - speedTime,
            })
        );
    }

    defineRandomTargetPoint() {
        const gameSize = this.scene.getGameSize();

        const target = new Vector(Math.random() * gameSize.width, Math.random() * gameSize.height);

        this.setTarget(target);
    }

    setTarget(target: Vector) {
        this.targetAngle = angleToDegrees(getAngleBetween(target, this.body));
    }

    shoot(angle: number) {
        const { body, bulletSpeed, weaponDamage } = this;

        this.scene.bullets.push(
            new SphereBullet(this.scene, new Vector(body.x, body.y), Owner.enemy, {
                speed: getOffsetToAngle(angle, bulletSpeed),
                angle,
                sprite: "laser-red",
                explodeSprite: "red-explode",
                damage: weaponDamage,
            })
        );
    }

    onOutOfWorld(nextTarget: Vector) {
        this.setTarget(nextTarget);
    }

    destroy() {
        const { scene, body } = this;

        scene.utils.tripleExplode(body.x, body.y);
        super.destroy();
    }
}
