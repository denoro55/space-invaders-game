import { CONFIG } from "config";
import { Vector } from "components";
import {
    getAngleBetween,
    angleToDegrees,
    getNearestAngle,
    angleToRadians,
    getOffsetToAngle,
    getCenterBody,
    getRandomInt,
} from "helpers";
import { GameScene } from "scenes/Game";
import { Owner, EnemyName } from "types";

import { Enemy } from "./Enemy";
import { SphereBullet } from "./../bullets";

interface IHunterParams {
    x: number;
    y: number;
}

enum Behavior {
    moving = "moving",
    stop = "stop",
    attack = "attack",
}

export class UFO extends Enemy {
    public body: Phaser.Types.Physics.Arcade.ImageWithDynamicBody;

    // shooting
    public weaponDamage: number = 10;
    private chanceToShoot: number = 0.25;
    private lastShootTime: number = 0;
    private shootIntervalTime: number = 1200;

    // attack
    private attackEndTime: number = 0;
    private attackIntervalTime: number = 1000;

    // direction
    private lastChangeTargetTime: number = 0;
    private targetPos: Vector;
    private targetAngle: number;
    private changeTargetInterval: number = 1000;
    private changeTargetIntervalRange: number = 2000;
    private rotateSpeed: number = 0.5;

    // speed
    private maxSpeed: number = 1;
    private speed: number = 1;
    private speedIncrease: number = 0.02;
    private stopEndTime: number = 0;
    private stopIntervalTime: number = 1500;
    private stopIntervalRangeTime: number = 2500;

    // base
    protected coinChance: number = 0.9;
    private bodySize: number = 36;
    private depth: number = 50;
    private scale: number = 0.7;
    protected hp: number = 70;
    private bulletSpeed: number = 7;

    private behavior: Behavior = Behavior.moving;

    constructor(scene: GameScene, params: IHunterParams) {
        super(scene, EnemyName.ufo);
        this.body = scene.physics.add.image(params.x, params.y, "enemy-ufo");

        this.init();

        this.defineTarget = this.defineTarget.bind(this);
        this.defineTarget();
    }

    init() {
        const { scale, depth, body, bodySize } = this;

        body.setScale(scale);
        body.setImmovable(true);
        body.setDepth(depth);

        const centerPos = getCenterBody(body.displayWidth, body.displayHeight, scale, bodySize);

        body.setCircle(bodySize, centerPos.x, centerPos.y);

        this.setStopEndTime(true);
    }

    act(time: number, delta: number) {
        this.update(time, delta);
    }

    update(_: number, delta: number) {
        const {
            scene,
            body,
            lastShootTime,
            shootIntervalTime,
            chanceToShoot,
            behavior,
            speedIncrease,
            changeTargetInterval,
            changeTargetIntervalRange,
            stopEndTime,
        } = this;
        const playerBody = scene.player.body;
        const time = scene.gameTime;

        this.updateBase(time, delta);

        switch (behavior) {
            case Behavior.moving: {
                if (this.speed < this.maxSpeed) {
                    this.speed += speedIncrease;
                }

                if (time > lastShootTime) {
                    if (Math.random() > 1 - chanceToShoot) {
                        this.shoot(
                            getAngleBetween(new Vector(playerBody.x, playerBody.y), new Vector(body.x, body.y)),
                            false
                        );
                    }

                    this.lastShootTime = time + shootIntervalTime;
                }

                if (time > this.lastChangeTargetTime) {
                    this.defineTarget();
                    const intervalTime = changeTargetInterval + Math.random() * changeTargetIntervalRange;
                    this.lastChangeTargetTime = time + intervalTime;
                }

                if (time > stopEndTime && !this.isOutsideScene()) {
                    this.behavior = Behavior.stop;
                }

                break;
            }

            case Behavior.stop: {
                if (this.speed > 0) {
                    this.speed -= speedIncrease;
                } else {
                    this.attackEndTime = time + this.attackIntervalTime;
                    this.behavior = Behavior.attack;
                }

                break;
            }

            case Behavior.attack: {
                if (time > this.attackEndTime) {
                    this.circleAttack();
                    this.setMovingBehavior();
                }

                break;
            }
        }
    }

    updateBase(time: number, delta: number) {
        const { body, angle, targetAngle, rotateSpeed } = this;

        const gameSize = this.scene.getGameSize();

        this.body.angle += this.speed;

        const angleInRadians = angleToRadians(angle);
        const deltaSpeed = delta / CONFIG.scene.delta;

        body.x += this.speed * Math.cos(angleInRadians) * deltaSpeed;
        body.y += this.speed * Math.sin(angleInRadians) * deltaSpeed;

        this.angle = getNearestAngle(angle, targetAngle, rotateSpeed);

        if (body.x < 100 || body.y < 100 || body.x > gameSize.width - 100 || body.y > gameSize.height - 100) {
            // need fix
            this.setTarget(new Vector(gameSize.width / 2, gameSize.height / 2));
        }
    }

    defineTarget() {
        const gameSize = this.scene.getGameSize();

        const target = new Vector(Math.random() * gameSize.width, Math.random() * gameSize.height);

        this.setTarget(target);
    }

    setTarget(target: Vector) {
        const { body } = this;

        this.targetPos = target;
        this.targetAngle = angleToDegrees(getAngleBetween(this.targetPos, body));
    }

    setStopEndTime(init: boolean = false) {
        const { scene, stopIntervalTime, stopIntervalRangeTime } = this;
        const time = scene.gameTime;

        this.stopEndTime = time + stopIntervalTime + getRandomInt(0, stopIntervalRangeTime);
    }

    setMovingBehavior() {
        const { scene, changeTargetInterval, shootIntervalTime } = this;
        const time = scene.gameTime;

        this.setStopEndTime();
        this.lastChangeTargetTime = time + changeTargetInterval;
        this.lastShootTime = time + shootIntervalTime;
        this.behavior = Behavior.moving;
    }

    circleAttack() {
        const angle = 360 / 8;
        const offset = Math.random() * angle;

        for (let i = 0; i < 8; i++) {
            this.shoot(angleToRadians(offset + i * angle), true);
        }
    }

    shoot(radians: number, isSphere = true) {
        const { body, bulletSpeed, weaponDamage } = this;

        this.scene.bullets.push(
            new SphereBullet(this.scene, new Vector(body.x, body.y), Owner.enemy, {
                speed: getOffsetToAngle(radians, bulletSpeed),
                angle: radians,
                sprite: isSphere ? "sphere-bullet" : "laser-red",
                explodeSprite: "red-explode",
                damage: weaponDamage,
                scale: isSphere ? 0.35 : 1,
                bodySize: isSphere ? 32 : 12,
            })
        );
    }

    destroy() {
        const { scene, body } = this;

        scene.utils.tripleExplode(body.x, body.y);
        this.circleAttack();

        super.destroy();
    }
}
