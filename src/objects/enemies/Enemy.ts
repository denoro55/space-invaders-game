import { Vector } from "components";
import { CONFIG } from "config";
import { GameScene } from "scenes/Game";
import { EnemyName } from "types";

export class Enemy {
    public body: Phaser.Types.Physics.Arcade.ImageWithDynamicBody;

    public name: EnemyName;
    protected scene: GameScene;
    protected hp: number;
    protected coinChance: number;
    protected isOutOfWorld: boolean = false;

    protected angle: number = 0;

    public weaponDamage: number;

    constructor(scene: GameScene, name: EnemyName) {
        this.scene = scene;
        this.name = name;
    }

    act(time: number, delta: number) {}

    updateWorldCollisions(time: number, delta: number) {
        const gameSize = this.scene.getGameSize();

        const { isOutOfWorld } = this;

        if (this.isOutsideScene(100)) {
            if (!isOutOfWorld) {
                const xRange = 250;
                const yRange = 150;

                const x = gameSize.width / 2 - xRange + Math.random() * (xRange * 2);
                const y = gameSize.height / 2 - yRange + Math.random() * (yRange * 2);

                const nextTarget = new Vector(x, y);

                this.onOutOfWorld(nextTarget);
            }

            this.isOutOfWorld = true;
        } else {
            this.isOutOfWorld = false;
        }
    }

    damage(value: number) {
        this.hp -= value;

        if (this.hp <= 0) {
            this.destroy();
        }
    }

    destroy() {
        const { scene, body, coinChance } = this;

        scene.enemies = scene.enemies.filter((b) => b !== this);
        body.destroy();

        const stats = scene.getStats();

        if (Math.random() > 1 - coinChance) {
            scene.utils.createBonus(body.x, body.y);
        }

        stats.kills[this.name] += 1;
        stats.totalKills += 1;

        scene.shakeScreen();
    }

    isOutsideScene(value: number = 0) {
        const { body } = this;

        const gameSize = this.scene.getGameSize();

        if (body.x < value || body.x > gameSize.width - value || body.y < value || body.y > gameSize.height - value) {
            return true;
        }

        return false;
    }

    onOutOfWorld(nextTarget: Vector) {}
}
