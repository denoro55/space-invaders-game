import { CONFIG } from "config";
import { Vector } from "components";
import {
    getAngleBetween,
    angleToDegrees,
    getNearestAngle,
    angleToRadians,
    getOffsetToAngle,
    getCenterBody,
    getAngleDiff,
    spreadRadians,
    getRandomInt,
} from "helpers";
import { GameScene } from "scenes/Game";
import { Owner, EnemyName } from "types";

import { Enemy } from "./Enemy";
import { SphereBullet } from "./../bullets";

interface IHunterParams {
    x: number;
    y: number;
}

enum Behavior {
    moving = "moving",
    stop = "stop",
    rotate = "rotate",
    attack = "attack",
}

export class Hunter extends Enemy {
    public body: Phaser.Types.Physics.Arcade.ImageWithDynamicBody;

    // shooting
    public weaponDamage: number = 10;
    private chanceToShoot: number = 0.25;
    private lastShootTime: number = 0;
    private shootIntervalTime: number = 1200;

    // attack
    private attackEndTime: number = 0;
    private attackIntervalTime: number = 3000;
    private attackShootIntervalTime: number = 325;
    private attackSpreadRange: number = 15;
    private attackHunting: boolean = true;

    // direction
    private lastChangeTargetTime: number = 0;
    private targetAngle: number;
    private changeTargetInterval: number = 1000;
    private changeTargetIntervalRange: number = 2000;
    private rotateSpeed: number = 2;
    private currentRotateSpeed: number = this.rotateSpeed;
    private rotateTimer: number = 0;
    private rotateIntervalTimer: number = 3000;

    // speed
    private maxSpeed: number = 2;
    private speed: number = 2;
    private speedIncrease: number = 0.05;
    private stopEndTime: number = 0;
    private stopIntervalTime: number = 3000;
    private stopIntervalRangeTime: number = 1000;

    // base
    protected coinChance: number = 0.9;
    private bodySize: number = 36;
    private depth: number = 50;
    private scale: number = 0.7;
    protected hp: number = 70;
    private bulletSpeed: number = 7;

    private behavior: Behavior = Behavior.moving;

    constructor(scene: GameScene, params: IHunterParams) {
        super(scene, EnemyName.hunter);
        this.body = scene.physics.add.image(params.x, params.y, "enemy-hunter-ship");

        this.init();

        this.defineTarget = this.defineTarget.bind(this);
        this.defineTarget();
    }

    init() {
        const { scale, depth, body, bodySize } = this;

        body.setScale(scale);
        body.setImmovable(true);
        body.setDepth(depth);

        const centerPos = getCenterBody(body.displayWidth, body.displayHeight, scale, bodySize);

        body.setCircle(bodySize, centerPos.x, centerPos.y);

        this.setStopEndTime(true);
    }

    act(time: number, delta: number) {
        this.update(time, delta);
    }

    update(_: number, delta: number) {
        const {
            scene,
            body,
            lastShootTime,
            shootIntervalTime,
            chanceToShoot,
            behavior,
            speedIncrease,
            stopEndTime,
            attackHunting,
            attackIntervalTime,
        } = this;
        const playerBody = scene.player.body;
        const time = scene.gameTime;

        this.updateBase(time, delta);

        switch (behavior) {
            case Behavior.moving: {
                if (this.speed < this.maxSpeed) {
                    this.speed += speedIncrease;
                }

                if (time > lastShootTime) {
                    if (Math.random() > 1 - chanceToShoot) {
                        this.shoot(getAngleBetween(new Vector(playerBody.x, playerBody.y), new Vector(body.x, body.y)));
                    }

                    this.lastShootTime = time + shootIntervalTime;
                }

                if (time > this.lastChangeTargetTime) {
                    this.updateTarget();
                }

                if (time > stopEndTime) {
                    this.behavior = Behavior.stop;
                }
                break;
            }

            case Behavior.stop: {
                if (this.speed > 0) {
                    this.speed -= speedIncrease;
                } else {
                    this.setRotateBehavior();
                }
                break;
            }

            case Behavior.rotate: {
                this.setTarget(new Vector(playerBody.x, playerBody.y));

                if (time > lastShootTime) {
                    this.shoot(angleToRadians(this.angle), this.attackSpreadRange);
                    this.lastShootTime = time + this.attackShootIntervalTime;
                }

                if (getAngleDiff(this.targetAngle, this.angle) <= 4) {
                    this.setAttackBehavior();
                } else if (time > this.rotateTimer) {
                    this.setMovingBehavior();
                }
                break;
            }

            case Behavior.attack: {
                if (attackHunting || time > this.attackEndTime - attackIntervalTime / 3) {
                    this.setTarget(new Vector(playerBody.x, playerBody.y));
                }

                if (this.speed < this.maxSpeed) {
                    this.speed += speedIncrease;
                }

                if (time > lastShootTime) {
                    this.shoot(angleToRadians(this.angle), this.attackSpreadRange);
                    this.lastShootTime = time + this.attackShootIntervalTime;
                }

                if (attackHunting && time > this.attackEndTime - attackIntervalTime / 1.5) {
                    this.attackHunting = false;
                    this.updateTarget();
                }

                if (time > this.attackEndTime) {
                    this.setMovingBehavior();
                }
                break;
            }
        }
    }

    updateBase(time: number, delta: number) {
        const { body, angle, targetAngle, currentRotateSpeed } = this;

        const gameSize = this.scene.getGameSize();

        this.body.angle = angle - 90;

        const angleInRadians = angleToRadians(angle);
        const deltaSpeed = delta / CONFIG.scene.delta;

        body.x += this.speed * Math.cos(angleInRadians) * deltaSpeed;
        body.y += this.speed * Math.sin(angleInRadians) * deltaSpeed;

        this.angle = getNearestAngle(angle, targetAngle, currentRotateSpeed);

        if (body.x < 100 || body.y < 100 || body.x > gameSize.width - 100 || body.y > gameSize.height - 100) {
            // need fix
            this.setTarget(new Vector(gameSize.width / 2, gameSize.height / 2));
        }
    }

    updateTarget() {
        const { scene, changeTargetInterval, changeTargetIntervalRange } = this;
        const time = scene.gameTime;

        this.defineTarget();
        const intervalTime = changeTargetInterval + Math.random() * changeTargetIntervalRange;
        this.lastChangeTargetTime = time + intervalTime;
    }

    defineTarget() {
        const gameSize = this.scene.getGameSize();

        const target = new Vector(Math.random() * gameSize.width, Math.random() * gameSize.height);

        this.setTarget(target);
    }

    setTarget(target: Vector) {
        const { body } = this;

        this.setTargetAngle(angleToDegrees(getAngleBetween(target, body)));
    }

    setTargetAngle(angle: number) {
        this.targetAngle = angle;
    }

    setAttackBehavior() {
        const { scene, rotateSpeed } = this;
        const time = scene.gameTime;

        this.attackEndTime = time + this.attackIntervalTime;
        this.currentRotateSpeed = rotateSpeed;
        this.attackHunting = true;
        this.behavior = Behavior.attack;
    }

    setStopEndTime(init: boolean = false) {
        const { scene, stopIntervalTime, stopIntervalRangeTime } = this;
        const time = scene.gameTime;

        if (init) {
            this.stopEndTime = time + 1000 + getRandomInt(0, stopIntervalTime);
        } else {
            this.stopEndTime = time + stopIntervalTime + getRandomInt(0, stopIntervalRangeTime);
        }
    }

    setMovingBehavior() {
        const { scene, changeTargetInterval, shootIntervalTime } = this;
        const time = scene.gameTime;

        this.setStopEndTime();
        this.lastChangeTargetTime = time + changeTargetInterval;
        this.lastShootTime = time + shootIntervalTime;
        this.behavior = Behavior.moving;
    }

    setRotateBehavior() {
        const { scene, rotateIntervalTimer, rotateSpeed } = this;
        const time = scene.gameTime;

        this.speed = 0;
        this.rotateTimer = time + rotateIntervalTimer;
        this.currentRotateSpeed = rotateSpeed + 1;
        this.behavior = Behavior.rotate;
    }

    shoot(radians: number, spreadRange: number = 0) {
        const { body, bulletSpeed, weaponDamage } = this;

        const angle = spreadRadians(radians, spreadRange);

        this.scene.bullets.push(
            new SphereBullet(this.scene, new Vector(body.x, body.y), Owner.enemy, {
                speed: getOffsetToAngle(angle, bulletSpeed),
                angle,
                sprite: "laser-red",
                explodeSprite: "red-explode",
                damage: weaponDamage,
            })
        );
    }

    destroy() {
        const { scene, body } = this;

        scene.utils.tripleExplode(body.x, body.y);

        super.destroy();
    }
}
