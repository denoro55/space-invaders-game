import { angleToRadians } from "helpers/angle";
import { CONFIG, isMobile } from "config";
import { Vector } from "components";
import { Owner } from "types";
import {
    angleToDegrees,
    getAngleBetween,
    getOffsetToAngle,
    getCenterBody,
    getDistanceBetweenPoints,
    getNearestAngle,
} from "helpers";

import { SphereBullet } from "./bullets";
import { GameScene } from "../scenes/Game";

interface IPlayerPointers {
    left: Phaser.Input.Pointer | null;
    right: Phaser.Input.Pointer | null;
}

export class Player {
    private scene: GameScene;
    public body: Phaser.Types.Physics.Arcade.ImageWithDynamicBody;

    // config
    private scale: number = 0.6;
    private bodySize: number = 26;
    private speed: number = 5;
    private bulletSpeed: number = 10;
    public lastShootTime: number = 0;
    private shootTime: number = 320;
    public hp: number = 100;
    public maxHp: number = 100;
    public weaponDamage: number = 10;
    // private laserSound: Phaser.Sound.BaseSound;
    // private lastFireTime: number = 0;

    pointers: IPlayerPointers = {
        left: null,
        right: null,
    };

    private angle: number = 0;
    private targetAngle: number = 0;
    private rotateSpeed: number = 5;

    constructor(scene: GameScene) {
        this.scene = scene;
        this.body = this.scene.physics.add.image(300, 300, "player-ship");
        this.lastShootTime = scene.gameTime + 100;
        // this.laserSound = scene.sound.add("laser-2");

        this.init();
    }

    init() {
        const { scene, scale } = this;

        this.body.setScale(scale);
        this.body.setDepth(100);
        this.body.setImmovable(true);

        scene.add.existing(this.body);
    }

    act(time: number, delta: number) {
        this.update(time, delta);
    }

    public update(time: number, delta: number) {
        if (!this.body.active) return;

        this.updateBody(time, delta);
        this.updateShooting(time, delta);
        this.updateMovement(time, delta);
        this.updateAngle(time, delta);
    }

    updateBody(time: number, delta: number) {
        const { scale, bodySize } = this;

        const offset = getOffsetToAngle(this.angle + Math.PI, 10);
        const pos = getCenterBody(this.body.displayWidth, this.body.displayHeight, scale, bodySize, offset);

        this.body.setCircle(bodySize, pos.x, pos.y);
    }

    updateShooting(time: number, delta: number) {
        const { scene, lastShootTime, body } = this;
        const { gameTime } = scene;

        if (isMobile) {
            if (gameTime > lastShootTime) {
                this.shoot(angleToRadians(this.body.angle - 90));
            }
        } else {
            const pointer = scene.input.activePointer;

            if (pointer.isDown) {
                const angle = getAngleBetween(new Vector(pointer.x, pointer.y), new Vector(body.x, body.y));
                this.shoot(angle);
            }
        }
    }

    updateMovement(time: number, delta: number) {
        const { scene, body } = this;
        const gameSize = this.scene.getGameSize();

        const deltaSpeed = delta / CONFIG.scene.delta;
        let spd = this.speed * deltaSpeed;

        if (isMobile) {
            if (this.pointers.left && this.pointers.left.isDown) {
                const pointer = this.pointers.left;
                const touchCenterPos = new Vector(CONFIG.touchAreaOffset, gameSize.height - CONFIG.touchAreaOffset);

                const dir = getAngleBetween(touchCenterPos, new Vector(pointer.x, pointer.y));
                const pointerPos = new Vector(pointer.x, pointer.y);
                const distanceToTouchCenter = Math.min(getDistanceBetweenPoints(pointerPos, touchCenterPos) / 35, 1);

                const resultSpeed = spd * distanceToTouchCenter;

                body.x -= resultSpeed * Math.cos(dir);
                body.y -= resultSpeed * Math.sin(dir);
            }
        } else {
            const cursorKeys = scene.input.keyboard.createCursorKeys();

            if (
                (cursorKeys.up.isDown || cursorKeys.down.isDown) &&
                (cursorKeys.right.isDown || cursorKeys.left.isDown)
            ) {
                spd *= 0.8;
            }

            if (cursorKeys.up.isDown) {
                body.y -= spd;
            } else if (cursorKeys.down.isDown) {
                body.y += spd;
            }

            if (cursorKeys.right.isDown) {
                body.x += spd;
            } else if (cursorKeys.left.isDown) {
                body.x -= spd;
            }
        }

        if (body.x < 0) body.x = 0;
        if (body.y < CONFIG.scene.minYPos) body.y = CONFIG.scene.minYPos;
        if (body.x > gameSize.width) {
            body.x = gameSize.width;
        }
        if (body.y > gameSize.height) {
            body.y = gameSize.height;
        }
    }

    updateAngle(time: number, delta: number) {
        const { scene, body, rotateSpeed } = this;
        const pointer = scene.input.activePointer;

        if (isMobile) {
            const gameSize = this.scene.getGameSize();

            if (this.pointers.right && this.pointers.right.isDown) {
                const pointer = this.pointers.right;
                const touchCenterPos = new Vector(
                    gameSize.width - CONFIG.touchAreaOffset,
                    gameSize.height - CONFIG.touchAreaOffset
                );

                this.targetAngle = getAngleBetween(new Vector(pointer.x, pointer.y), touchCenterPos);
            }

            this.angle = angleToRadians(getNearestAngle(angleToDegrees(this.angle), angleToDegrees(this.targetAngle), rotateSpeed));
        } else {
            this.angle = getAngleBetween(new Vector(pointer.x, pointer.y), new Vector(body.x, body.y));
        }

        this.body.angle = angleToDegrees(this.angle) + 90;
    }

    shoot(angle: number) {
        const { scene, body, bulletSpeed, weaponDamage } = this;
        const { gameTime } = scene;

        if (gameTime > this.lastShootTime) {
            // this.laserSound.play();

            this.scene.bullets.push(
                new SphereBullet(this.scene, new Vector(body.x, body.y), Owner.player, {
                    speed: getOffsetToAngle(angle, bulletSpeed),
                    angle,
                    sprite: "laser-green",
                    explodeSprite: "green-explode",
                    damage: weaponDamage,
                })
            );

            this.lastShootTime = gameTime + this.shootTime;
        }
    }

    damage(value: number) {
        this.hp -= value;

        if (this.hp <= 0) {
            this.destroy();
        }
    }

    setPointer(pos: keyof IPlayerPointers, pointer: Phaser.Input.Pointer) {
        this.pointers[pos] = pointer;
    }

    destroy() {
        const { scene, body } = this;

        scene.utils.tripleExplode(body.x, body.y);
        body.destroy();

        scene.onFail();
        scene.shakeScreen();
    }

    // updateFire() {
    //   const { scene, body, angle, lastFireTime } = this;
    //   const time = scene.gameTime;

    //   const invertedAngle = angle + Math.PI;
    //   const fireDistance = 45;
    //   const posOffset = 0.4;

    //   if (time > lastFireTime) {
    //     scene.effects.push(
    //       new Fire(
    //         scene,
    //         new Vector(
    //           body.x + Math.cos(invertedAngle + posOffset) * fireDistance,
    //           body.y + Math.sin(invertedAngle + posOffset) * fireDistance
    //         ),
    //         {
    //           angle: invertedAngle,
    //         }
    //       )
    //     );
    //     scene.effects.push(
    //       new Fire(
    //         scene,
    //         new Vector(
    //           body.x + Math.cos(invertedAngle - posOffset) * fireDistance,
    //           body.y + Math.sin(invertedAngle - posOffset) * fireDistance
    //         ),
    //         {
    //           angle: invertedAngle,
    //         }
    //       )
    //     );
    //     this.lastFireTime = time + 75;
    //   }
    // }
}
