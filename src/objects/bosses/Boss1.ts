let graphics: Phaser.GameObjects.Graphics;

interface IParams {
  scene: Phaser.Scene;
  x: number;
  y: number;
}

export class Boss1 {
  private scene: Phaser.Scene;
  public body: Phaser.Geom.Rectangle;
  public maxHp: number = 100;
  public hp: number = 100;

  constructor({ scene, x, y }: IParams) {
    this.scene = scene;
    this.body = new Phaser.Geom.Rectangle(x, y, 150, 150);

    graphics = scene.add.graphics({
      lineStyle: { width: 2, color: 0xffffff },
      fillStyle: { color: 0xff0000 },
    });
  }

  draw() {
    const { scene, body } = this;

    graphics.strokeRectShape(body);
  }

  update() {}

  damage() {
    this.hp -= 1;
  }
}
