import { GameScene } from "scenes/Game";
import { Vector } from "components";

import { Bonus } from "./Bonus";

const COIN_MAP = {
    0: {
        sprite: "coin-bronze",
        cost: 5,
    },
    1: {
        sprite: "coin-silver",
        cost: 7,
    },
    2: {
        sprite: "coin-gold",
        cost: 10,
    },
};

const getCoinType = () => {
    const value = Math.random();

    if (value > 0.5) return 0;
    if (value > 0.2) return 1;
    return 2;
};

export class Coin extends Bonus {
    body: Phaser.Types.Physics.Arcade.ImageWithDynamicBody;

    private cost: number;
    private creationTime: number;
    private lifeTime: number = 5000;

    constructor(scene: GameScene, pos: Vector) {
        super(scene);

        const coinParams = COIN_MAP[getCoinType()];
        this.cost = coinParams.cost;
        this.body = this.scene.physics.add.image(pos.x, pos.y, coinParams.sprite);
        this.creationTime = scene.gameTime;

        this.init();
    }

    init() {
        this.body.setDepth(10);
        this.body.setCircle(16);

        this.initCollisions();
    }

    initCollisions() {
        const { scene } = this;

        scene.physics.add.collider(this.body, scene.player.body, () => {
            scene.score += this.cost;
            this.destroy();
        });
    }

    act() {
        this.update();
    }

    update() {
        const { scene, body } = this;
        const time = scene.gameTime;

        body.angle += Math.sin(time * 0.003) * 3;

        if (time > this.creationTime + this.lifeTime) {
            this.destroy();
        }
    }
}
