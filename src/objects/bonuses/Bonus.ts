import { GameScene } from "scenes/Game";

export class Bonus {
    scene: GameScene;
    body: Phaser.Types.Physics.Arcade.ImageWithDynamicBody;

    constructor(scene: GameScene) {
        this.scene = scene;
    }

    act() {}

    destroy() {
        const { body } = this;

        this.scene.bonuses = this.scene.bonuses.filter((b) => b !== this);
        body.destroy();
    }
}
