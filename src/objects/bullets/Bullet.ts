import { Vector } from "components/Vector";
import { CONFIG } from "config";
import { GameScene } from "scenes/Game";
import { Owner } from "types";

export class Bullet {
    scene: GameScene;
    body: Phaser.Types.Physics.Arcade.ImageWithDynamicBody;

    protected pos: Vector;
    protected depth: number;
    protected owner: Owner;
    protected damage: number;

    constructor(scene: GameScene, pos: Vector, owner: Owner, damage: number) {
        this.scene = scene;
        this.pos = pos;
        this.owner = owner;
        this.damage = damage;
    }

    act(time: number, delta: number) {}

    update(time: number, delta: number) {
        const { body } = this;

        const gameSize = this.scene.getGameSize();

        if (body.x < 0 || body.x > gameSize.width || body.y < 0 || body.y > gameSize.height) {
            this.destroy();
        }
    }

    initCollisions() {
        const { scene, owner, damage } = this;

        if (owner === Owner.player) {
            scene.enemies.forEach((enemy) => {
                scene.physics.add.collider(this.body, enemy.body, () => {
                    this.explode();
                    enemy.damage(damage);
                });
            });
        } else {
            scene.physics.add.collider(this.body, scene.player.body, () => {
                this.explode();
                scene.player.damage(damage);
            });
        }
    }

    explode() {}

    destroy() {
        const { body } = this;

        this.scene.bullets = this.scene.bullets.filter((b) => b !== this);
        body.destroy();
    }
}
