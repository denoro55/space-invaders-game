import { Vector } from "components";
import { CONFIG } from "config";
import { angleToDegrees, getOffsetToAngle, getCenterBody } from "helpers";
import { GameScene } from "scenes/Game";
import { Owner } from "types";

import { Explode } from "../effects";
import { Bullet } from "./Bullet";

interface ISphereParams {
    damage: number;
    speed: Vector;
    angle: number;
    sprite: string;
    explodeSprite: string;
    scale?: number;
    bodySize?: number;
}

const DEFAULT_PARAMS = {
    scale: 1,
    bodySize: 6,
};

export class Sphere extends Bullet {
    body: Phaser.Types.Physics.Arcade.ImageWithDynamicBody;
    params: ISphereParams;

    protected depth = 10;

    constructor(scene: GameScene, pos: Vector, owner: Owner, params: ISphereParams) {
        super(scene, pos, owner, params.damage);
        this.body = scene.physics.add.image(pos.x, pos.y, params.sprite);

        this.params = {
            ...DEFAULT_PARAMS,
            ...params,
        };

        this.init();
    }

    init() {
        const { body, scene, params, depth } = this;
        const { angle, scale } = params;

        body.setAngle(angleToDegrees(angle) + 90);
        body.setDepth(depth);
        body.setOrigin(0.5, 0.5);
        body.setScale(scale);

        this.initBody();
        this.initCollisions();

        scene.add.existing(this.body);
    }

    initBody() {
        const { params } = this;
        const { angle, scale, bodySize } = params;

        const offset = getOffsetToAngle(angle, this.body.displayHeight / scale / 3);
        const pos = getCenterBody(this.body.displayWidth, this.body.displayHeight, scale, bodySize, offset);

        this.body.setCircle(bodySize, pos.x, pos.y);
    }

    act(time: number, delta: number) {
        this.update(time, delta);
    }

    draw() {}

    explode() {
        const { body, scene, params } = this;

        scene.effects.push(
            new Explode(scene, new Vector(body.x, body.y), {
                sprite: params.explodeSprite,
            })
        );

        this.destroy();
    }

    update(time: number, delta: number) {
        const { body } = this;
        const { speed } = this.params;

        body.x += speed.x * (delta / CONFIG.scene.delta);
        body.y += speed.y * (delta / CONFIG.scene.delta);

        super.update(time, delta);
    }
}
