import { GameScene } from './../../scenes/Game/Game';
import { CONFIG } from "config";
import { getRandomInt } from "helpers";

interface ICometParams {}

export class Comet {
    scene: GameScene;
    body: Phaser.GameObjects.Image;

    speed: number = 3;

    constructor(scene: GameScene, params: ICometParams) {
        const gameSize = scene.getGameSize();

        this.scene = scene;
        this.body = this.scene.add.image(getRandomInt(0, gameSize.width), getRandomInt(0, gameSize.height), "comet");

        this.updateParams();
    }

    updateParams() {
        const { body } = this;

        body.setAlpha(0.03 + Math.random() * 0.1);
        body.setScale(0.2, 0.75 + Math.random() * 0.5);
        this.speed = getRandomInt(3, 7);
    }

    refreshPosition() {
        const { body } = this;

        const gameSize = this.scene.getGameSize();

        body.x = getRandomInt(0, gameSize.width);
        body.y = -100;
    }
}
