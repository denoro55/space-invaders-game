import { GameScene } from "scenes/Game";

interface IStarParams {
    initialAlphaCoef: number;
}

export class Star {
    scene: GameScene;
    body: Phaser.GameObjects.Sprite;
    initialCoef: number;
    lastRelocationTime: number;

    constructor(scene: GameScene, params: IStarParams) {
        const gameSize = scene.getGameSize();

        this.scene = scene;
        this.body = scene.add.sprite(Math.random() * gameSize.width, Math.random() * gameSize.height, "star");

        this.initialCoef = params.initialAlphaCoef;

        this.body.setDepth(0);
        this.body.setScale(0.25 + Math.random() * 0.25);
    }

    act(time: number, delta: number) {
        this.update(time, delta);
    }

    update(time: number, delta: number) {
        const { scene, body, initialCoef, lastRelocationTime } = this;
        const gameTime = scene.gameTime;
        body.angle += 4;
        body.alpha = Math.sin((gameTime + initialCoef) / 1500);

        const gameSize = this.scene.getGameSize();

        if (gameTime > lastRelocationTime && body.alpha <= 0.5) {
            body.x = Math.random() * gameSize.width;
            body.y = Math.random() * gameSize.height;
            this.lastRelocationTime = gameTime + 1000;
        }
    }
}
