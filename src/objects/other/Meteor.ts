import { GameScene } from "scenes/Game";
import { Vector } from "components";

interface IAsteroidParams {
    alpha: number;
    spriteIndex: number;
    scale: number;
}

export class Meteor {
    scene: GameScene;
    body: Phaser.GameObjects.Sprite;
    public speed: Vector;

    constructor(scene: GameScene, pos: Vector, speed: Vector, params: IAsteroidParams) {
        this.scene = scene;
        this.body = scene.add.sprite(pos.x, pos.y, `meteor-${params.spriteIndex}`);
        this.speed = speed;
        this.body.setAlpha(params.alpha);
        this.body.setDepth(5);
        this.body.setScale(params.scale, params.scale);
    }
}
