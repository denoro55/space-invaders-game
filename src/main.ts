import "phaser";
import { getGameConfig, INITIAL_GAME_DATA } from "config/game";
import { isDev, isMobile } from "config";

import {
    isLandscapeOrientation,
    showOrientationWarning,
    hideOrientationWarning,
    showFullscreen,
    getCurrentOrientation,
} from "./helpers";

const ORIENTATION_UPDATE_TIMEOUT_DELAY = 200;

let game: Game;
let isGameInited = false;
let orientation: TOrientation = null;

export class Game extends Phaser.Game {
    public data = INITIAL_GAME_DATA;

    constructor(config: Phaser.Types.Core.GameConfig) {
        super(config);
    }
}

function tryInitGame() {
    if (!isGameInited) {
        if (isLandscapeOrientation() || !isMobile) {
            game = new Game(getGameConfig());
            isGameInited = true;

            if (isDev) {
                console.log("Game inited", game);
            } else {
                console.log("Game inited");
            }
        } else {
            console.log("Game could not be inited as it does not support portrait orientation");
        }
    }
}

function updateOrientation(attempts = 120) {
    setTimeout(() => {
        const nextOrientation = getCurrentOrientation();

        if (nextOrientation !== orientation) {
            orientation = nextOrientation;

            tryInitGame();

            if (isLandscapeOrientation()) {
                hideOrientationWarning();
            } else if (isMobile) {
                showOrientationWarning();

                if (game) {
                    showFullscreen(game.canvas, false);
                }
            }
        } else {
            if (attempts > 0) {
                window.requestAnimationFrame(() => {
                    updateOrientation(attempts - 1);
                });
            }
        }
    }, ORIENTATION_UPDATE_TIMEOUT_DELAY);
}

function handleOrientationChange(ev: Event) {
    updateOrientation();
}

function init() {
    if (isMobile) {
        // document.body.style.backgroundColor = "#000";
    }
}

window.addEventListener("load", handleOrientationChange);
window.addEventListener("orientationchange", handleOrientationChange);

init();
