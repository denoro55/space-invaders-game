export interface IGlobalData {
    level: number;
    levels: Record<
        number,
        {
            score: number;
        }
    >;
    score: number;
}

export enum Owner {
    player = "player",
    enemy = "enemy",
}

export enum GameMode {
    totalKills = "totalKills",
    enemyKills = "enemyKill",
    time = "time",
    waves = "waves",
}

export enum EnemyName {
    bot = "bot",
    ufo = "ufo",
    hunter = "hunter",
    flyer = "flyer",
}

export enum SceneName {
    menu = "MenuScene",
    preview = "PreviewScene",
    game = "GameScene",
    fail = "FailScene",
    win = "WinScene",
}
