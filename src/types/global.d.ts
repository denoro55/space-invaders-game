export {};

declare global {
    interface Document {
        mozCancelFullScreen?: () => Promise<void>;
        msExitFullscreen?: () => Promise<void>;
        webkitExitFullscreen?: () => Promise<void>;
        mozFullScreenElement?: Element;
        msFullscreenElement?: Element;
        webkitFullscreenElement?: Element;
    }

    interface HTMLElement {
        msRequestFullscreen?: (options: FullscreenOptions) => Promise<void>;
        mozRequestFullscreen?: (options: FullscreenOptions) => Promise<void>;
        webkitRequestFullscreen?: (options: FullscreenOptions) => Promise<void>;
        exitFullscreen?: () => Promise<void>;
        webkitExitFullscreen?: () => Promise<void>;
        msExitFullscreen?: () => Promise<void>;
    }

    type PartialRecord<K extends keyof any, T> = {
        [P in K]?: T;
    };

    type TOrientation = "horizontal" | "vertical";
}
