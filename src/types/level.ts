import { Enemy } from "objects/enemies";
import { EnemyName, GameMode } from "./game";

export interface TotalKillsMode {
    type: GameMode.totalKills;
    target: number;
}

export interface EnemyKillsMode {
    type: GameMode.enemyKills;
    target: PartialRecord<EnemyName, number>;
}

export interface TimeMode {
    type: GameMode.time;
    target: number;
}

export interface WavesMode {
    type: GameMode.waves;
    target: {
        maxEnemies: number;
        enemies: (new (...args: any[]) => Enemy)[];
    }[];
}

interface WavesModeState {
    waveNumber: number;
    enemies: (new (...args: any[]) => Enemy)[];
    maxEnemies: number;
}

export type ILevelState = WavesModeState;

export type LevelMode = TotalKillsMode | EnemyKillsMode | TimeMode | WavesMode;

export interface ILevelConfig {
    level: number;
    title: string;
    text: string;
    mission: string;
    mode: LevelMode;
  }